<p align="center"><img src="public/img/brand/blue.png"></p>

## Sobre el proyecto
Es una aplicación que gestiona las citas realizadas por los pacientes y atendidas por su respectivo medico.

Esta aplicación se tomo como base el curso [Curso intensivo de Laravel y Android](https://github.com/JCarlosR/my-appointments) en donde se desarrollo una aplicación web con su correspondiente version móvil.

## Licencia
Es un proyecto open-source bajo la licencia [MIT](https://opensource.org/licenses/MIT).
