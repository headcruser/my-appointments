<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'v1'], function ()
{
    # AUTENTICACION
    Route::post('/auth/login', 'AuthController@login');
    Route::post('/auth/register', 'AuthController@register');

    #ESPECIALIDADES
    Route::get('specialties', 'SpecialtyController@index');
    Route::get('specialties/{specialty}/doctors', 'SpecialtyController@doctors');

    # HORARIOS
    Route::get('schedule/hours', 'ScheduleController@hours'); # schedule/hours?date=2019-09-09&doctor_id=2

    Route::group(['middleware' => ['jwt.auth']] ,function() {
        # PERFIL DE USUARIO
        Route::get('/user', 'UserController@show');
	    Route::post('/user', 'UserController@update');

        # CERRAR SESION
        Route::get('/auth/logout', 'AuthController@logout');

        # APPOINTMENTS (CITAS)
        Route::post('/appointments',"AppointmentController@store");
        Route::get('/appointments', "AppointmentController@index");

        # FCM
        Route::post('/fcm/token', 'FirebaseController@token');
    });
});
