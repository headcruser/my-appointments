<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes
|
*/

Auth::routes();

# -------------------------------------------------------
#  ROUTES FOR DASHBOARD
# -------------------------------------------------------
Route::get('/', 'HomeController@index')
    ->middleware('auth')
    ->name('home');


# -------------------------------------------------------
#  ROUTES FOR ADMINISTRATION
# -------------------------------------------------------
Route::group(['middleware'=>['auth']],  function()
{
    # PROFILE
    Route::get('perfil', 'ProfileController@index')->name('profile');
    Route::post('perfil', 'ProfileController@store')->name('profile.update');


    # RESOURCE ADMINISTRATOR
    Route::middleware(['role:admin'])->namespace('Admin')->group( function() {

        # -------------------------------------------------------
        #  SETTINGS
        # -------------------------------------------------------
        Route::get('/settings', 'SettingController@index')->name('settings');
        Route::post('/settings', 'SettingController@store')->name('settings.store');

        # -------------------------------------------------------
        #  USERS
        # -------------------------------------------------------

        Route::resource('users', 'UserController')->only([ 'index','store','update','destroy']);

        # -------------------------------------------------------
        #  ROLES AND PERMISSIONS
        # -------------------------------------------------------
        Route::resource('roles', 'RoleController')->only([ 'index','store','update','destroy']);

        Route::post('rol-permisos/{rol}', [
            'as'            => 'rol.permisos',
            'middleware'    => ['auth'],
            'uses'          => 'RoleController@attachPermission'
        ]);

        Route::resource('permissions', 'PermisionController')->only(['index','store','update','destroy']);

        # -------------------------------------------------------
        #  SPECIALITIES
        # -------------------------------------------------------
        Route::resource('specialties', 'SpecialityController')->parameters([
            'specialties' => 'speciality'
        ])->only([ 'index','store','update','destroy']);

        # -------------------------------------------------------
        #  DOCTORS
        # -------------------------------------------------------
        Route::resource('doctors', 'DoctorController')->only([ 'index','store','update','destroy']);

        # -------------------------------------------------------
        #  PATIENTS
        # -------------------------------------------------------
        Route::resource('patients', 'PatientController')->only([ 'index','store','update','destroy']);


        # -------------------------------------------------------
        #  CHARTS
        # -------------------------------------------------------
        Route::get('charts/appointments/line',[
            'as'    => 'appointment.line',
            'uses'  =>  'ChartController@appointments'
        ]);

        Route::get('charts/appointments/column', [
            'as'    => 'appointment.column',
            'uses'  =>  'ChartController@doctors'
        ]);

        Route::post('charts/appointments/column/data', [
            'as'    => 'appointment.data',
            'uses'  =>  'ChartController@doctorsJson'
        ]);

        # -------------------------------------------------------
        #  FCM
        # -------------------------------------------------------
        Route::post('/fcm/send','FirebaseController@sendAll')->middleware('auth');
    });

    # RESOURCE DOCTOR
    Route::middleware(['role:doctor'])->namespace('Doctor')->group( function() {
        Route::get('schedule', [
            'as'            => 'schedule',
            'uses'          => 'ScheduleController@edit'
        ]);
        Route::post('schedule', [
            'as'            => 'schedule',
            'uses'          => 'ScheduleController@store'
        ]);
    });

    # RESOURCE PATIENT
    Route::middleware(['role:patient|doctor|admin'])->namespace('Patient')->group(function () {

        # EXIGIR TELEFONO
        Route::group(['middleware' => ['phone']], function () {
             # CREACION CITAS MEDICAS
            Route::get('appointments/create', [
                'as'            => 'appointments.create',
                'uses'          => 'AppointmentController@create'
            ]);

            # CREAR CITA AJAX
            Route::post('appointments', [
                'as'            => 'appointments.store',
                'uses'          => 'AppointmentController@store'
            ]);
        });


        # DETALLE DE CITA
        Route::get('appointments/{appointment}', [
            'as'            => 'appointments.show',
            'uses'          => 'AppointmentController@show'
        ]);

        # LISTADO DE CITAS
        Route::get('appointments', [
            'as'            => 'appointments.index',
            'uses'          => 'AppointmentController@index'
        ]);

        # CANCELACION DE CITA
        Route::post('appointments/{appointment}/cancel', [
            'as'            => 'appointments.cancel',
            'uses'          => 'AppointmentController@cancel'
        ]);

        # CONFIRMACION DE CITA
        Route::post('appointments/{appointment}/confirm', [
            'as'            => 'appointments.confirm',
            'uses'          => 'AppointmentController@confirm'
        ]);

    });

});


