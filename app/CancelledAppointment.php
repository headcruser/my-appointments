<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CancelledAppointment extends Model
{
    public function cancelledBy()
    {
        # Belongs to Cancellation N-1 User Has Many
        return $this->belongsTo(User::class,'cancelled_by');
    }
}
