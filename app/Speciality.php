<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Speciality extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "specialities";

    const ID = 'id';
    const NAME = 'name';
    const DESCRIPTION = 'description';

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 7;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DESCRIPTION
    ];

    /**
     * Filtra la coleccion del modelo
     *
     * @return mixed
     */
    public static function filtered()
    {
        $field = request()->get('field') ?? self::ID;
        $sort  = request()->get('sort')  ?? 'desc';

        return self::orderBy($field,$sort)->paginate();
    }

    public function users(){
        return $this->belongsToMany(User::class);
    }


    /**
     * Obtiene una coleccion de elementos del modelo
     *
     * @return Illuminate\Support\Collection
     */
    public static function getListSelect()
    {
        return self::orderBy(self::NAME, 'asc')
            ->pluck(self::NAME, self::ID);
    }
}
