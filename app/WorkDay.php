<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

class WorkDay extends Model
{
    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */

    protected $fillable = [
        'day',
        'active',
        'morning_start',
        'morning_end',
        'afternoon_start',
        'afternoon_end',
        'user_id',
    ];

    /**
     * Get Format Date
     *
     * @param  string  $value
     * @return string
     */
    public function getMorningStartAttribute($value)
    {
        return (new Carbon($value))->format('g:i A');
    }

    /**
     * Get Format Date
     *
     * @param  string  $value
     * @return string
     */
    public function getMorningEndAttribute($value)
    {
        return (new Carbon($value))->format('g:i A');
    }
    /**
     * Get Format Date
     *
     * @param  string  $value
     * @return string
     */
    public function getAfternoonStartAttribute($value)
    {
        return (new Carbon($value))->format('g:i A');
    }

    /**
     * Get Format Date
     *
     * @param  string  $value
     * @return string
     */
    public function getAfternoonEndAttribute($value)
    {
        return (new Carbon($value))->format('g:i A');
    }

    public function scopeActive($query) {
        return $query->where('active',true);
    }
}
