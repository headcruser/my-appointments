<?php

namespace App\Http\Responsables;

use App\Permission;
use Illuminate\Contracts\Support\Responsable;
use Illuminate\Http\Request;

class RoleShow implements Responsable
{
    /**
     * @var $collection
     * Colecion de elementos filtrados
     */
    protected $collection;

    const TABLE_COMPONENT = 'admin.roles.partials._table';

    const VIEW_INDEX_COMPONENT = 'admin.roles.index';

    /**
     * RolShow constructor.
     * @param  $collection
     */
    public function __construct($collection)
    {
        $this->collection = $collection;
    }

    /**
     * Create an HTTP response that represents the object.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function toResponse($request)
    {
        $data = $this->buildData($request);
        $aditionalData = $this->addAditionalData();

        return view(self::VIEW_INDEX_COMPONENT, array_merge($data,$aditionalData));
    }

    /**
     * Obtiene la plantilla a renderizar
     */
    public function getTemplate($request)
    {
        return (string) view(self::TABLE_COMPONENT, $this->buildData($request));
    }

    /**
     * Construye la informacion a mostrar en la vista
     *
     * @param Request $request
     * @return array
     */
    protected function buildData(Request $request)
    {
        return [
            # Mecanismo datatables
            'roles'         => $this->collection,
            'permissions'   => Permission::all()
        ];
    }
    /**
     * Agrega elementos adicionales a la vista
     *
     * @return array
     */
    public function addAditionalData():array
    {
        return [
            # Se incluye la tabla a renderizar
            'table_component'       => self::TABLE_COMPONENT,

            # Seccion de modal
            'model_route'            => route('roles.index'),
        ];
    }
}
