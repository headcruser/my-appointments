<?php

namespace App\Http\Requests;

use App\Interfaces\ScheduleServiceInterface;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;

class StoreAppointment extends FormRequest
{
    private $scheduleService; 

    public function __construct(ScheduleServiceInterface $scheduleService) {
        $this->scheduleService = $scheduleService;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description'       => 'required',
            'specialties'       => 'exists:specialities,id',
            'doctors'           => 'exists:users,id',
            'scheduled_date'    => 'required',
            'scheduled_time'    => 'required',
            'type'              => 'required'
        ];
    }

    /**
     * Get the error messages for the defined validation rules
     *
     * @return array
     */
    public function messages()
    {
        return  [
            'scheduled_time.required' => 'Por favor seleccione una hora válida para su cita'
        ];
    }

    public function withValidator($validator) 
    {
        $validator->after(function($validator) 
        {
            $date = $this->input('scheduled_date');
            $doctorId = $this->input('doctors');
            $scheduledTime = $this->input('scheduled_time');

            if (!$date || !$doctorId || !$scheduledTime) {
                return;
            }

            $start = new Carbon($scheduledTime);

            if (!$this->scheduleService->isAvalibleInterval($date, $doctorId, $start)) 
            {
                $validator
                    ->errors()
                    ->add('avalible_time', 'La hora seleccionada ya se encuentra reservada por otro paciente');
            }

        });
    }
}
