<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dni'       => 'required',
            'address'   => 'required',
            'phone'     => 'required',
            'photo'     => 'nullable'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'dni.required'      => 'El Campo DNI es requerido',
            'address.required'  => 'El campo Direccion es requerido',
            'phone.required'    => 'El campo Telefono es requerido',
        ];
    }
}
