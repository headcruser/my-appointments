<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProfile;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;

use Storage;

class ProfileController extends Controller
{
    /**
     * Display Profile
     */
    public function index()
    {
        return view('profile',[
            'authUser' => auth()->user()
        ]);
    }

    /**
     * Update Data profile user
     *
     * @param StoreProfile $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreProfile $request)
    {
        $user = auth()->user();

        if ($request->has('photo') )
        {
            $image = $request->file('photo');
            $fileName =$user->id.'_'.time() . '.' . $image->getClientOriginalExtension();

            $img = Image::make($image->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->stream();
            Storage::disk('local')->put("public/users/{$fileName}" , $img, 'public');
            Storage::delete("public/users/$user->photo");
            $user->photo = $fileName;
        }

         # Guardar Informacion del usuario
         $user->dni = $request->input('dni');
         $user->address = $request->input('address');
         $user->phone = $request->input('phone');
         $user->save();

        # Peticion Ajax
        return response()->json([
            'success'   => true,
            'message'   => 'Perfil actualizado correctamente',
            'user'      => $user,
        ]);
    }
}
