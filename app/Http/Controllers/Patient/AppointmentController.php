<?php

namespace App\Http\Controllers\Patient;

use App\Appointment;
use App\CancelledAppointment;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAppointment;
use App\Interfaces\ScheduleServiceInterface;
use App\Speciality;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Validator;

class AppointmentController extends Controller
{
    public function index()
    {
        return view('appointments.index',[
            'pendingCollection'     => Appointment::reserved()->paginate(),
            'confirmedCollection'   => Appointment::confirmed()->paginate(),
            'historyCollection'     => Appointment::cancelled()->paginate()
        ]);
    }

    public function show(Appointment $appointment){
        return view('appointments.show',compact('appointment'));
    }

    public function create(ScheduleServiceInterface $scheduleService)
    {
        $specialties = Speciality::pluck('name', 'id')->prepend('Seleccina una especialidad', '');
        $specialtyId = old('specialties');
        $doctors = collect(['' => 'Selecciona antes una especialidad']) ;

        if ($specialtyId) {
            $specialty = Speciality::find($specialtyId);
            $doctors = $specialty->users->pluck('name','id'); # Revisar especialidad sin usuarios
        }

        $scheduledDate = old('scheduled_date');
        $doctorId = old('doctors');

        if($scheduledDate && $doctorId) {
            $intervals = $scheduleService->getAvalibleIntervals($scheduledDate,$doctorId);
        }else{
            $intervals = null;
        }

        return view('appointments.create',[
            'specialties'   => $specialties,
            'doctors'       => $doctors,
            'intervals'     => $intervals
        ]);
    }

    public function store(StoreAppointment $request)
    {
        $created = Appointment::createForPatient($request,auth()->id());

        if($created)
            $notificacion = 'La cita se ha registrado correctamente';
        else
            $notificacion = 'Ocurrio un problema al registrar la cita';

        return back()->with([
            'notification' => $notificacion
        ]);

    }

    public function cancel(Request $request,Appointment $appointment)
    {
        if ( $request->has('justification' ) ) {
            $cancellation = new CancelledAppointment();
            $cancellation->justification = $request->input('justification');
            $cancellation->cancelled_by = auth()->id();

            $appointment->cancellation()->save($cancellation);
        }

        $appointment->status = 'Cancelada';
        $saved = $appointment->save();

        if ($saved)
            $appointment->patient->sendFCM('Su cita ha sido cancelada.');

        return response()->json([
            'message'           => 'La Cita ha sido cancelada correctamente',

            'templateConfirmed' => (string) view('appointments.tables._confirmed', [
                'confirmedCollection'   => Appointment::confirmed()->paginate(),
            ]),

            'templatePending'  => (string) view('appointments.tables._pending', [
                'pendingCollection'   => Appointment::reserved()->paginate(),
            ]),

            'templateHistory'  => (string) view('appointments.tables._history', [
                'historyCollection'   => Appointment::cancelled()->paginate()
            ]),
        ]);
    }

    public function confirm(Request $request, Appointment $appointment)
    {
        $appointment->status = 'Confirmada';
        $saved = $appointment->save(); // update

        if ($saved)
            $appointment->patient->sendFCM('Su cita se ha confirmado!');

        return response()->json([
            'message'           => 'La Cita se ha confirmado correctamente',

            'templateConfirmed' => (string) view('appointments.tables._confirmed', [
                'confirmedCollection'   => Appointment::confirmed()->paginate(),
            ]),

            'templatePending'  => (string) view('appointments.tables._pending', [
                'pendingCollection'   => Appointment::reserved()->paginate(),
            ]),

            'templateHistory'  => (string) view('appointments.tables._history', [
                'historyCollection'   => Appointment::cancelled()->paginate()
            ]),
        ]);
    }
}
