<?php

namespace App\Http\Controllers\Doctor;

use App\Enums\DaysOfWeek;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WorkDay;
use Carbon\Carbon;


class ScheduleController extends Controller
{
    public function edit()
    {
        $workDays = WorkDay::where('user_id', auth()->id())->get();
        $days = DaysOfWeek::getDays();

        if($workDays->isEmpty()){
           foreach($days as $day){
               $workDays->push((new WorkDay()));
           }
        }

        return view('schedule',[
            'days'      => $days,
            'workDays'  => $workDays
        ]);
    }


    public function store(Request $request)
    {
       $active = $request->input('active') ?: [];
       $morning_start = $request->input('morning_start');
       $morning_end = $request->input('morning_end');
       $afternoon_start = $request->input('afternoon_start');
       $afternoon_end = $request->input('afternoon_end');

       $days = DaysOfWeek::getDays();
       
       $errors = [];
       foreach($days as $key => $day) {
           if($morning_start[$key] > $morning_end[$key]){
                $errors [] = "Las horas del turno mañana son inconsistentes para el dia {$day}.";
           }

            if ($afternoon_start[$key] > $afternoon_end[$key]) {
                $errors[] = "Las horas del turno tarde son inconsistentes para el dia {$day}.";
            }
           
           WorkDay::updateOrCreate([
                'day' => $key,
                'user_id'  => auth()->id()
           ],[
               'active'             => in_array($key,$active),
               'morning_start'      => Carbon::createFromFormat('H:i', $morning_start[$key]),
               'morning_end'        => Carbon::createFromFormat('H:i', $morning_end[$key] ),
               'afternoon_start'    => Carbon::createFromFormat('H:i', $afternoon_start[$key] ),
               'afternoon_end'      => Carbon::createFromFormat('H:i', $afternoon_end[$key]),
           ]);
       }

       if(count($errors) > 0){
            return back()->with(compact('errors'));
       }
       return back()->with([
           'notification' => 'Los cambios se han guardado correctamente'
       ]);
    }
}
