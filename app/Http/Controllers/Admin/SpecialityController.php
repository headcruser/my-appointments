<?php

namespace App\Http\Controllers\Admin;

use App\Speciality;
use App\Http\Controllers\Controller;
use App\Http\Responsables\SpecialityShow;

use Illuminate\Http\Request;

class SpecialityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index(Request $request)
    {
        $collection = Speciality::filtered();
        $responsable = new SpecialityShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        Speciality::create($request->all());

        $collection = Speciality::filtered();
        $responsable = new SpecialityShow($collection);

        return response()->json([
            'message'   => 'Especialidad Creada correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Speciality  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , Speciality $speciality)
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        $speciality->update($request->all());

        $collection = Speciality::filtered();
        $collection->setPath(route('specialties.index'));

        $responsable = new SpecialityShow($collection);

        return response()->json([
            'message'   => 'Especialidad Actualizada correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Destroy the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Speciality  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , Speciality $speciality)
    {
        $speciality->delete();

        $collection = Speciality::filtered();
        $collection->setPath(route('specialties.index'));

        $responsable = new SpecialityShow($collection);

        return response()->json([
            'message'   => 'Especialidad eliminada correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }
}
