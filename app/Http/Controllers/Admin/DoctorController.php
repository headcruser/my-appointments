<?php

namespace App\Http\Controllers\Admin;

use App\{Role,User};
use App\Http\Controllers\Controller;
use App\Http\Responsables\DoctorShow;

use Illuminate\Http\Request;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = User::doctors()->paginate();

        $responsable = new DoctorShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'bail|required|min:2',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
        ]);

        $request->merge(['password' => bcrypt($request->get('password'))]);

         // Create the user
         if ( $user = User::create($request->except('roles')) )
         {
            $user->roles()->detach();
            $roleDoctor = Role::doctor()->first();

            if (isset($roleDoctor) ){
                $user->roles()->attach($roleDoctor);
            }

           
            $user->specialties()->attach($request->input('specialties'));

            $collection = User::doctors()->paginate();

            $responsable = new DoctorShow($collection);

            return response()->json([
                'data'      => $request->all(),
                'message'   => 'Doctor Creado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        } else {
            return response()->json([
                'message' => 'No se pudo crear al Doctor'
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, User $doctor)
    {
        $this->validate($request,[
            'name'  => 'bail|required|min:2',
            'email' => 'required|email',
        ]);

        // Update user

        $data = $request->except('password');
        $password = $request->input('password');

        // check for password change
        if (!empty($password)) {
            $data['password'] = bcrypt($password);
        }

        $doctor->fill($data) ;
        $doctor->save();

        $doctor->specialties()->sync($request->input('specialties'));

        $collection = User::doctors()->paginate();
        $collection->setPath(route('doctors.index'));

        $responsable = new DoctorShow($collection);

        return response()->json([
            'message'   => 'Doctor Actualizado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , User $doctor)
    {
        # Check Role Doctor
        if ( auth()->id() == $doctor->id ) {
            return response()->json([
                'message'   => 'No puedes eliminar al usuario actual',
            ],500);
        }
        if( $doctor->delete() )
        {
            $collection = User::doctors()->paginate();
            $collection->setPath(route('doctors.index'));

            $responsable = new DoctorShow($collection);

            return response()->json([
                'message'   => 'Doctor eliminado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        } else {
            return response()->json([
                'message'   => 'El doctor no pudo ser eliminado',
            ],500);
        }
    }
}
