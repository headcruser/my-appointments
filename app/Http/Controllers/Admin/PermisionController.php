<?php

namespace App\Http\Controllers\Admin;

use App\Permission;
use App\Http\Responsables\PermissionShow;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class PermisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Support\Responsable
    */
    public function index(Request $request)
    {
        $collection = Permission::orderBy('id','desc')->paginate();
        $responsable = new PermissionShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        Permission::create($request->all());

        $collection = Permission::orderBy('id','desc')->paginate();
        $responsable = new PermissionShow($collection);

        return response()->json([
            'message'   => 'Especialidad Creada correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , Permission $permission)
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        $permission->fill($request->all());
        $permission->save();

        $collection = Permission::orderBy('id','desc')->paginate();
        $collection->setPath(route('permissions.index'));

        $responsable = new PermissionShow($collection);

        return response()->json([
            'message'   => 'Permiso Actualizado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Destroy the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Speciality  $recepcion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , Permission $permission)
    {
        $permission->delete();

        $collection = Permission::orderBy('id','desc')->paginate();
        $collection->setPath(route('permissions.index'));

        $responsable = new PermissionShow($collection);

        return response()->json([
            'message'   => 'Permiso eliminado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }
}
