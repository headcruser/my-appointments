<?php

namespace App\Http\Controllers\Admin;

use App\Role;
use App\Http\Responsables\RoleShow;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RoleController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = Role::with('perms')->orderBy('id','desc')->paginate();

        $responsable = new RoleShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request )
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        Role::create($request->all());

        $collection = Role::with('perms')->orderBy('id','desc')->paginate();
        $responsable = new RoleShow($collection);

        return response()->json([
            'message'   => 'Rol Creado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , Role $role)
    {
        $rules = [
            'name'  => 'required|min:3'
        ];

        $messages = [
            'name.required' => 'Es necesario ingresar un nombre',
            'name.min'      => 'Como minimo, el nombre debe tener 3 caracteres'
        ];

        $this->validate($request,$rules,$messages);

        $role->fill($request->all());
        $role->save();

        $collection = Role::with('perms')->orderBy('id','desc')->paginate();
        $collection->setPath(route('roles.index'));

        $responsable = new RoleShow($collection);

        return response()->json([
            'message'   => 'Permiso Actualizado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Destroy the specified resource in storage.
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , Role $role)
    {
        $role->delete();

        $collection = Role::with('perms')->orderBy('id','desc')->paginate();
        $collection->setPath(route('roles.index'));

        $responsable = new RoleShow($collection);

        return response()->json([
            'message'   => 'Rol eliminado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Sync Permission for the Role
     *
     * @param  Request  $request
     * @param  Role  $role
     * @return \Illuminate\Http\Response
    */
    public function attachPermission(Request $request, Role $rol)
    {
        $rol->perms()->detach();
        $rol->attachPermission(request()->get('permissions'));

        $collection = Role::with('perms')->orderBy('id','desc')->paginate();
        $collection->setPath(route('roles.index'));

        return response()->json([
            'message' => 'Permisos agregados correctamente',
            'template'  => (new RoleShow($collection))->getTemplate($request)
        ]);
    }
}
