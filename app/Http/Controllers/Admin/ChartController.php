<?php

namespace App\Http\Controllers\Admin;

use App\Appointment;
use App\Http\Controllers\Controller;
use App\User;
use DB;
use Carbon\Carbon;

use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function appointments()
    {
        $montlyCounts = Appointment::select(
                DB::raw('MONTH(created_at) as month'),
                DB::raw('COUNT(1) as citas')
            )->groupBy('month')->get()->makeHidden(['scheduled_time_12']);

        $appointmentsByMounts = array_fill(0,12,0);

        foreach($montlyCounts as $montlyCount){
            $appointmentsByMounts[ $montlyCount['month'] - 1 ] = $montlyCount['citas'];
        }

        return view('charts.appointments',compact('appointmentsByMounts'));
    }

    public function doctors()
    {
        $now = Carbon::now();

        return view('charts.doctors',[
            'end'   => $now->format('Y-m-d'),
            'start' => $now->subYear()->format('Y-m-d'),
        ]);
    }

    public function doctorsJson(Request $request)
    {
        $start = $request->input('start');
        $end = $request->input('end');

        $doctors = User::doctors()
            ->select('name')
            ->withCount([
                'attendendAppointments' => function($query) use ($start,$end) {
                    $query->whereBetween('scheduled_date',[$start,$end]);
                },
                'cancelledAppointments' => function($query) use ($start, $end) {
                    $query->whereBetween('scheduled_date', [$start, $end]);
                }
            ])
            ->orderByDesc('attendend_appointments_count')
            ->get()
            ->take(3);


        $data = [
            'categories'    => $doctors->pluck('name')->toArray(),
            'series'        => [
                [
                    'name' => 'Citas Atendidas',
                    'data' =>  $doctors->pluck('attendend_appointments_count')->toArray()
                ],
                [
                    'name' => 'Citas canceladas',
                    'data' => $doctors->pluck('cancelled_appointments_count')->toArray()
                ]
            ],
            'date' =>$request->all()
        ];

        return $data;
    }
}
