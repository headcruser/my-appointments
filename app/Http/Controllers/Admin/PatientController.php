<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Role;

use App\Http\Controllers\Controller;
use App\Http\Responsables\PatientShow;

use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = User::patients()->paginate();

        $responsable = new PatientShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'bail|required|min:2',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
        ]);

        $request->merge(['password' => bcrypt($request->get('password'))]);

         // Create the user
         if ( $user = User::create($request->except('roles')) )
         {
            $user->roles()->detach();
            $rolePatient = Role::patient()->first();

            if (isset($rolePatient) ){
                $user->roles()->attach($rolePatient);
            }

            $collection = User::patients()->paginate();

            $responsable = new PatientShow($collection);

            return response()->json([
                'data'      => $request->all(),
                'message'   => 'Paciente Creado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        } else {
            return response()->json([
                'message' => 'No se pudo crear al paciente'
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function update(Request $request, User $patient)
    {
        $this->validate($request,[
            'name'  => 'bail|required|min:2',
            'email' => 'required|email',
        ]);

        $data = $request->except('password');
        $password = $request->input('password');

        // check for password change
        if (!empty($password)) {
            $data['password'] = bcrypt($password);
        }

        $patient->fill($data);
        $patient->save();

        $collection = User::patients()->paginate();
        $collection->setPath(route('patients.index'));

        $responsable = new PatientShow($collection);

        return response()->json([
            'message'   => 'Paciente Actualizado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , User $patient)
    {
        # Check Role Doctor
        if ( auth()->user()->id == $patient->id ) {
            return response()->json([
                'message'   => 'No puedes eliminar al usuario actual',
            ],500);
        }
        try
        {
            $patient->delete();
            
            $collection = User::patients()->paginate();
            $collection->setPath(route('patients.index'));

            $responsable = new PatientShow($collection);

            return response()->json([
                'message'   => 'Paciente eliminado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        }catch(\Exception $e) {
            return response()->json([
                'message'   => $e->getMessage(),
            ],500);
        }
    }
}
