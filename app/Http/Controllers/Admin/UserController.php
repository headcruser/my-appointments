<?php

namespace App\Http\Controllers\Admin;

use App\User;
use App\Http\Responsables\UserShow;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $collection = User::with('roles')
            ->orderBy(User::ID,'desc')
            ->paginate();

        $responsable = new UserShow($collection);

        if ($request->ajax())  {
            return response()->json([
                'template' => $responsable->getTemplate($request)
            ]);
        }
        return $responsable->toResponse($request);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'      => 'bail|required|min:2',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:6',
            'roles'     => 'required|min:1'
        ]);


        $request->merge(['password' => bcrypt($request->get('password'))]);

         // Create the user
         if ( $user = User::create($request->except('roles', 'permissions')) )
         {
            $roles = $request->get('roles',[]);

            foreach ($roles as $role_user_in) {
                $user->attachRole($role_user_in);
            }

            $collection = User::with('roles')
            ->orderBy(User::ID,'desc')
            ->paginate();

            $responsable = new UserShow($collection);

            return response()->json([
                'data'      => $request->all(),
                'message'   => 'Usuario Creado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        } else {
            return response()->json([
                'message' => 'No se pudo crear al usuario'
            ], 422);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $this->validate($request,[
            'name'  => 'bail|required|min:2',
            'email' => 'required|email|unique:users,email,' . $user->id,
            'roles' => 'required|min:1'
        ]);

        // Update user
        $user->fill($request->except('roles', 'permissions', 'password'));

         // check for password change
        if($request->get('password')) {
            $user->password = bcrypt($request->get('password'));
        }

        $user->roles()->detach();


        $roles = $request->get('roles');

		if (isset($roles) ){
            $user->roles()->attach($roles);
        }

        $user->save();


        $collection = User::with('roles')
            ->orderBy(User::ID,'desc')
            ->paginate();

        $collection->setPath(route('users.index'));

        $responsable = new UserShow($collection);

        return response()->json([
            'message'   => 'Usuario Actualizado correctamente',
            'template'  => $responsable->getTemplate($request)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  User  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request , User $user)
    {
        if ( auth()->user()->id == $user->id ) {
            return response()->json([
                'message'   => 'No puedes eliminar al usuario actual',
            ],500);
        }
        if( $user->delete() ) {
            $collection = User::with('roles')
            ->orderBy(User::ID,'desc')
            ->paginate();
            $collection->setPath(route('users.index'));

            $responsable = new UserShow($collection);

            return response()->json([
                'message'   => 'Usuario eliminado correctamente',
                'template'  => $responsable->getTemplate($request)
            ]);
        } else {
            return response()->json([
                'message'   => 'El usuario no pudo ser eliminado',
            ],500);
        }
    }
}
