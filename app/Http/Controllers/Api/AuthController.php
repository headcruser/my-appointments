<?php

namespace App\Http\Controllers\Api;

use App\User;
use App\Http\Controllers\Controller;
use App\Http\Traits\ValidateAndCreatePatient;

use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator;


class AuthController extends Controller
{
    use ValidateAndCreatePatient;

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email','password');

        $validator = Validator::make($credentials, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'success'   => false,
                'message'   => 'Error Datos login',
                'errors'    => $validator->messages()
            ],422);
        }

         try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json([
                    'success'   => false ,
                    'error'     => 'Sus datos no coninciden con nuestros registros.'
                ], 401);
            }
        } catch (JWTException $e) {
            return response()->json([
                'success'   => false,
                'error'     => 'Fallo el inicio de sesion, por favor intente otra vez.'
            ], 500);
        }

        return response()->json([
            'success'   => true,
            'token'     => $token,
            'user'      => User::where('email',$credentials['email'])->first()
        ]);
    }

    /**
     * API Login, on success return JWT Auth token
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        $token = JWTAuth::getToken();

        try {
            JWTAuth::invalidate($token);

            return response()->json([
                'success'   => 'true',
                'message'   => 'Sesión Finalizada'
            ]);
        } catch(JWTException $ex){
            return response()->json([
                'success'   => 'false',
                'message'   => 'Fallo el cierre de sesion, por favor reintente'
            ],422);
        }
    }

    /**
     * Registro de pacientes
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        return $this->login($request);
    }
}
