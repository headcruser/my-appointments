<?php

namespace App\Http\Controllers\Api;

use Auth;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class FirebaseController extends Controller
{
    public function token(Request $request)
    {
        if(!$request->has('device_token')) {
            return;
        }

        $user = Auth::guard('api')->user();
        $user->device_token = $request->input('device_token');
        $user->save();
    }
}
