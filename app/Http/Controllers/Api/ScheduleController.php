<?php

namespace App\Http\Controllers\Api;

use App\WorkDay;
use App\Http\Controllers\Controller;
use App\Interfaces\ScheduleServiceInterface;

use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    public function hours(Request $request, ScheduleServiceInterface $scheduleService)
    {
        $this->validate($request,[
            'date'      => 'required|date_format:Y-m-d',
            'doctor_id' => 'required|exists:users,id'
        ]);
        $doctorId = $request->input('doctor_id');
        $date = $request->input('date');

        return $scheduleService->getAvalibleIntervals($date,$doctorId);
    }

}
