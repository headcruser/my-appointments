<?php

namespace App\Http\Controllers\Api;

use App\Appointment;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAppointment;

use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    public function index()
    {
        $user = Auth::guard('api')->user();

        return $user->asPatientAppointments()
            ->with([
                'specialty' => function($query){
                    return $query->select('id','name');
                },
                'doctor'    => function($query){
                    return $query->select('id','name');
                }
            ])
            ->get([
                'id',
                'description',
                'specialty_id',
                'doctor_id',
                'scheduled_date',
                'scheduled_time',
                'type',
                'status',
                'created_at',
            ]);
    }

    public function store(StoreAppointment $request) 
    {
        $patientID = Auth::guard("api")->id();
        $appointment =  Appointment::createForPatient($request, $patientID);
        
        return [
            'success' => ($appointment) ? true: false
        ];
    }
}
