<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Speciality;

class SpecialtyController extends Controller
{
    public function index()
    {
        return Speciality::all(['id','name']);
    }

    public function doctors(Speciality $specialty)
    {
        return $specialty->users()->get(['users.name', 'users.id']);
    }
}
