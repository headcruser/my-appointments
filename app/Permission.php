<?php

namespace App;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    const ID = 'id';
    const NAME = 'name';
    const DISPLAY_NAME = 'display_name';
    const DESCRIPTION = 'description';

    const DEFAULT_PERMISSIONS = [
        'view_specialties',
        'add_specialties',
        'edit_specialties',
        'delete_specialties',

        'view_doctors',
        'add_doctors',
        'edit_doctors',
        'delete_doctors',

        'view_patients',
        'add_patients',
        'edit_patients',
        'delete_patients',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DISPLAY_NAME,
        self::DESCRIPTION
    ];

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 7;

}
