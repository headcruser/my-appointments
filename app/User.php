<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

use Cache;
use Config;
use Storage;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable,EntrustUserTrait;

    const ID = 'id';
    const NAME = 'name';
    const EMAIL = 'email';
    const PASSWORD = 'password';
    const DNI = 'dni';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::ID        ,
        self::NAME      ,
        self::EMAIL     ,
        self::PASSWORD  ,
        self::DNI       ,
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'pivot',
        'created_at',
        'updated_at',
        'email_verified_at'
    ];

    /**
     * Rules for create Users
     *
     * @var array
     */
    public static $rules = [
        self::NAME      => 'required|string|max:255',
        self::EMAIL     => 'required|string|email|max:255|unique:users',
        self::PASSWORD  => 'required|string|min:6|confirmed',
    ];

     /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 7;


    /**
     * Display Name Role for user
     *
     * @return string Return Name
     */
    public function displayNameRole()
    {
        return $this->cachedRoles()->pluck('name')->implode(' ');
    }


    /**
     * Big block of caching functionality.
     *
     * @return mixed Roles
     */
    public function cachedRoles()
    {
        $userPrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_roles_for_user_'.$this->$userPrimaryKey;
        return Cache::remember($cacheKey, Config::get('cache.ttl') , function () {
            return $this->roles()->get();
        });
    }



    /**
     * Obtiene la imagen de perfil
     *
     * @return void
     */
    public function getAvatarUrl()
    {
        if (empty($this->photo))
            return asset('img/theme/avatar.png');

        return Storage::url('users'.DIRECTORY_SEPARATOR.$this->photo);
    }

    ############################################################################
    # RELATIONS FOR MODEL
    ############################################################################

    /**
     * Obtiene las especialidades asociadas a un usuario
     *
     * @return QueryBuilder
     */
    public function specialties()
    {
        return $this->belongsToMany(Speciality::class)->withTimestamps();
    }

    /**
     * Obtiene los doctores asociados al usuario
     *
     * @return void
     */
    public function asDoctorAppointments()
    {
        return $this->hasMany(Appointment::class,'doctor_id');
    }

    /**
     * Obtiene las citas atentidads para el usuario especificado
     *
     * @return QueryBuilder
     */
    public function attendendAppointments()
    {
        return $this->asDoctorAppointments()->where('status','Atendida');
    }

    /**
     * Obtiene las citas medicas canceladas
     *
     * @return QueryBuilder
     */
    public function cancelledAppointments()
    {
        return $this->asDoctorAppointments()->where('status','Cancelada');
    }

    /**
     * Obtiene los pacientes para un usuario especificado
     *
     * @return void
     */
    public function asPatientAppointments()
    {
        return $this->hasMany(Appointment::class, 'patient_id');
    }

    ############################################################################
    # SCOPES FOR MODEL
    ############################################################################

    /**
     * Obtiene la lista de doctores
     *
     * @param [type] $query
     * @return void
     */
    public function scopeDoctors($query)
    {
        return $query->withRole('doctor');
    }

    /**
     * Obtiene la lista pacientes
     *
     * @param [type] $query
     * @return Querybuilder
     */
    public function scopePatients($query)
    {
        return $query->withRole('patient');
    }

    /**
     * Obtiene la lista de administradores
     *
     * @param [type] $query
     * @return QueryBuilder
     */
    public function scopeAdmins($query)
    {
        return $query->withRole('admin');
    }

    ############################################################################
    # JWT PACKAGE METHODS
    ############################################################################
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    ############################################################################
    # STATIC METHODS FOR MODEL
    ############################################################################

    /**
     * Create Patients
     *
     * @param array $data
     * @return void
     */
    public static function createPatient(array $data)
    {
        $patient =  self::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
        ]);

        $rolePatient = Role::patient()->first();
        $patient->attachRole($rolePatient);

        return $patient;
    }

    /**
     * Envio de notificacion FCM para el usuario especificado
     *
     * @param string $message Mensaje al usuario
     * @return void
     */
    public function sendFCM(string $message)
    {
        if (!$this->device_token)
            return;

        return fcm()->to([
            $this->device_token
        ])->notification([
            'title' => config('app.name'),
            'body'  => $message
        ])->send();
    }

}
