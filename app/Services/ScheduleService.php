<?php namespace App\Services;

use App\Appointment;

use App\Interfaces\ScheduleServiceInterface;

use App\WorkDay;

use Carbon\Carbon;

class ScheduleService implements ScheduleServiceInterface
{
    public function isAvalibleInterval($date,$doctorId, Carbon $start)
    {
        $exists =  Appointment::where('doctor_id',$doctorId)
        ->where('scheduled_date',$date)
        ->where('scheduled_time',$start->format('H:i:s'))
        ->exists();

        return !$exists; # Avalible if already none exist
    }
    public function getAvalibleIntervals($date, $doctorId)
    {
        $workDays = WorkDay::where('active',true)
            ->where('day', $this->getDayFromDate($date) )
            ->where('user_id',$doctorId)
            ->first([
                'morning_start','morning_end',
                'afternoon_start','afternoon_end'
            ]);

        if (empty($workDays)) {
            return [
                'morning'   => [],
                'afternoon' => []
            ];
        }

        $workDays = $workDays->getOriginal();

        return [
            'morning'   => $this->getIntervals($workDays['morning_start'], $workDays['morning_end'], $date ,$doctorId ),
            'afternoon' => $this->getIntervals($workDays['afternoon_start'], $workDays['afternoon_end'],$date ,$doctorId)
        ];
    }

    public function getDayFromDate($date)
    {
        $day = new Carbon($date);

        # DAY OF WEEK
        # CARBON :  0-SUNDAY -> 6-SATURDAY
        # WORKDAY : 0-MODAY -> 6-SUNDAY
        $dayFormatCarbon = $day->dayOfWeek;
        $day = ($dayFormatCarbon == 0 ? 6: $dayFormatCarbon -1 );

        return $day;
    }

    private function getIntervals(string $strStart ,string $strEnd, $date, $doctorId,  int $period = 30)
    {
        $start = new Carbon($strStart);
        $end = new Carbon($strEnd);

        $intervals = [];

        while ($start < $end) {

            $interval = [];

            $interval['start'] = $start->format('g:i A');
            $avalible = $this->isAvalibleInterval($date,$doctorId,$start);

            $start->addMinute($period);
            $interval['end'] = $start->format('g:i A');

            if( $avalible ){
                $intervals[] = $interval;
            }
        }

        return $intervals;
    }
}
