<?php

use Illuminate\Support\Facades\Route;

if (!function_exists('routeIs')) {
    /**
     * Regresa una clase css , si la ruta especificada esta siendo visitada.
     *
     * @param string|array $route Nombre que se le asigno a la ruta
     * @param string $class Clase css que se utiliza para resaltar el link
     * @return string Regresa el nombre de la clase
     * @see Illuminate\Routing\Router::is
     */
    function routeIs($route, string $class = 'active text-info'): string
    {
        return !Route::is($route) ?: $class;
    }
}