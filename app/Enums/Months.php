<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static OptionOne()
 * @method static static OptionTwo()
 * @method static static OptionThree()
 */
final class Months extends Enum
{
    const Enero = 1;
    const Febrero = 2;
    const Marzo = 3;
    const Abril = 4;
    const Mayo = 5;
    const Junio = 6;
    const Julio = 7;
    const Agosto = 8;
    const Septiembre = 9;
    const Octubre = 10;
    const Noviembre = 11;
    const Diciembre = 12;
}
