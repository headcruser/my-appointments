<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

final class DaysOfWeek extends Enum
{
    public static function getDays():array
    {
        return [
            __("Monday"),
            __("Tuesday"),
            __("Wednesday"),
            __("Thursday"),
            __("Friday"),
            __("Saturday"),
            __("Sunday")
        ];
    }
}
