<?php namespace App\Interfaces;

use Carbon\Carbon;

interface ScheduleServiceInterface {
    public function isAvalibleInterval($date,$doctorId, Carbon $start);
    public function getAvalibleIntervals($date,$doctorId);
}

