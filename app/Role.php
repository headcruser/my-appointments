<?php

namespace App;

use Zizaco\Entrust\EntrustRole;
use Cache;
use Config;

class Role extends EntrustRole
{
    const ID = 'id';
    const NAME = 'name';
    const DISPLAY_NAME = 'display_name';
    const DESCRIPTION = 'description';

    const DEFAULT_ROLES = [
        'admin',
        'doctor',
        'patient'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::NAME,
        self::DISPLAY_NAME,
        self::DESCRIPTION
    ];

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 7;

    public function scopeDoctor($query) {
        return $query->where('name','doctor');
    }

    public function scopePatient($query) {
        return $query->where('name','patient');
    }

    public function scopeAdmin($query)
    {
        return $query->where('name', 'admin');
    }

    /**
     * Obtiene una coleccion de elementos del modelo
     *
     * @return Illuminate\Support\Collection
     */
    public static function getListSelect()
    {
        return self::orderBy(self::DISPLAY_NAME, 'asc')
            ->pluck(self::DISPLAY_NAME, self::ID);
    }


    public function cachedPermissions()
    {
        $rolePrimaryKey = $this->primaryKey;
        $cacheKey = 'entrust_permissions_for_role_' . $this->$rolePrimaryKey;

        return Cache::remember($cacheKey, Config::get('cache.ttl') , function () {
            return $this->perms()->get();
        });
    }
}
