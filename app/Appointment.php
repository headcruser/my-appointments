<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Appointment extends Model
{
    protected $fillable = [
        'description',
        'specialty_id',
        'doctor_id',
        'patient_id',
        'scheduled_date',
        'scheduled_time',
        'type'
    ];

    protected $hidden = [
        'specialty_id','doctor_id', 'scheduled_time',
    ];

    protected $appends = [
        'scheduled_time_12'
    ];

    # RELATIONS

    /**
     * N Appointment->specialty 1
     *
     * @return void
     */
    public function specialty(){
        return $this->belongsTo(Speciality::class)->withDefault([
            'name' => 'S/Especialidad'
        ]);
    }

    /**
     * N Appointment->doctor (1) 
     *
     * @return void
     */
    public function doctor()
    {
        return $this->belongsTo(User::class)->doctors()->withDefault([
            'name' => 'S/Doctor'
        ]);
    }

    /**
     * N Appointment->doctor (1) 
     *
     * @return void
     */
    public function patient()
    {
        return $this->belongsTo(User::class)->patients()->withDefault([
            'name' => 'S/patient'
        ]);
    }

    /**
     * 
     * HasOne     belongsTo
     * 1        - 1/0
     * $appointment->cancelation->justification
     * @return void
     */
    public function cancellation ()
    {
        return $this->hasOne(CancelledAppointment::class);
    }

    # ACCESSORS 
    public function getScheduledTime12Attribute()
    {
        return Carbon::createFromFormat('H:i:s', $this->scheduled_time)->format('g:i A');
    }

    # SCOPES
    public function scopeCurrentAuth($query)
    {
        if(auth()->user()->hasRole('doctor')){
            return $query->where('doctor_id', auth()->id());
        }

        if (auth()->user()->hasRole('patient')) {
            return $query->where('patient_id', auth()->id());
        }

        return $query;
    }

    public function scopeReserved($query)
    {
        return $query->with(['specialty', 'doctor'])
            ->where('status', 'Reservada')
            ->currentAuth();
    }

    public function scopeConfirmed($query)
    {
        return $query->with(['specialty', 'doctor'])
            ->where('status','Confirmada')
            ->currentAuth();
    }

    public function scopeCancelled($query)
    {
        return $query->with(['specialty', 'doctor'])
            ->whereIn('status', ['Atendida', 'Cancelada'])
            ->currentAuth();
    }

    # STATIC METHODS
    static public function createForPatient(Request $request ,$patiendId)
    {
        return self::create(
            [
                'description'       => $request->input('description'),
                'specialty_id'      => $request->input('specialty_id'),
                'doctor_id'         => $request->input('doctor_id'),
                'patient_id'        => $patiendId,
                'scheduled_date'    => $request->input('scheduled_date'),
                'scheduled_time'    => Carbon::createFromFormat('g:i A', $request->input('scheduled_time'))
                                        ->format('H:i:s'),

                'type'              => $request->input('type'),
            ]
        );
    }
}
