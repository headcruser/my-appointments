<?php
/**
 * EXAMPLE STRUCTURE
 *  'elements' => [
 *   [
 *       'type' => 'text',                   // input fields type
 *       'data' => 'string',                 // data type, string, int, boolean
 *       'name' => 'app_name',               // unique name for field
 *       'label' => 'App Name',              // you know what label it is
 *       'rules' => 'required|min:2|max:50', // validation rule of laravel
 *       'class' => '',                      // any class for input
 *       'value' => 'CoolApp'                // default value if you want
 *   ]
 */
return
[
    'app' =>
    [
        'title' => 'General',
        'desc'  => 'Nombre general de la aplicacion',
        'icon'  => 'ni ni-bold-down',

        'elements' => [
            [
                'type' => 'text',
                'data' => 'string',
                'name' => 'app_name',
                'label' => 'App Name',
                'rules' => 'required|min:2|max:50',
                'class' => '',
                'value' => 'CoolApp'
            ]
        ]
    ],
    'email' =>
    [
        'title' => 'Email',
        'desc'  => 'Correo Electronico de configuracion para la aplicación',
        'icon'  => 'ni ni-email-83',

        'elements' =>
        [
            [
                'type' => 'email',
                'data' => 'string',
                'name' => 'email',
                'label' => 'E-mail',
                'rules' => 'required|min:2|max:50',
                'class' => '',
                'value' => 'administrador@gmail.com'
            ],
        ]
    ],
];
