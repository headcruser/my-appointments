<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PermissionSeeder::class,
            RoleSeeder::class,
            UsersSeeder::class,
            SpecialitySeeder::class,
            WorkDaysTableSeeder::class,
            AppointmentsTableSeeder::class
        ]);
    }
}
