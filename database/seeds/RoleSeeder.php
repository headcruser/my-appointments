<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Role::DEFAULT_ROLES;

        foreach ($roles as $perms) {
            Role::firstOrCreate([
                'name'         => $perms,
                'display_name' => camel_case($perms),
                'description'  => 'description ' . $perms
            ]);
        }
    }
}
