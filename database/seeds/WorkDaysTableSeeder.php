<?php

use App\Enums\DaysOfWeek;
use App\WorkDay;
use Illuminate\Database\Seeder;

class WorkDaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $days = DaysOfWeek::getDays();

       
        foreach ($days as $key => $day) {

            WorkDay::create([
                'day'               => $key,
                'active'            => ($key == 3),
                'morning_start'     => ($key == 3) ? '07:00:00' : '05:00:00',
                'morning_end'       => ($key == 3) ? '09:30:00' : '05:00:00',

                'afternoon_start'   => ($key == 3) ? '15:00:00' : '13:00:00',
                'afternoon_end'     => ($key == 3) ? '18:00:00' : '13:00:00',

                'user_id'           => 2,
            ]);
        }   
    }
}
