<?php

use App\{
    Role,
    Speciality,
    User,
};

use Illuminate\Database\Seeder;

class SpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $specialties = [
            'Angiologia'    => 'Especialidad para curar la ansiedad',
            'Dermatologia'  => 'Especialidad para el cuidado de la piel',
            'Oftamologia'   => 'Especialidad para el cuidado de los dientes',
            'Traumatologia' => 'Especialidad para los grandes accidentes',
        ];


        foreach($specialties as $specialtyName => $description)
        {
            $specialty = Speciality::create([
                'name'          => $specialtyName,
                'description'   => $description
            ]);

            $userDoctor = User::doctors()->get();

            $specialty->users()->saveMany($userDoctor);
        }

    }
}
