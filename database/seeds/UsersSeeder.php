<?php

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # ROLES
        $roleAdmin = Role::admin()->first();
        $roleDoctor = Role::doctor()->first();
        $rolePatient = Role::patient()->first();

        # ADMINISTRADOR
        $userAdmin = factory(User::class)->create([
            'name'              => "Administrador",
            'email'             => "administrador@gmail.com",
            'password'          => bcrypt('123456')
        ]);
        $userAdmin->attachRole($roleAdmin);
        $roleAdmin->attachPermissions(Permission::all());

         # DOCTOR
        $userDoctorDefault = factory(User::class)->create([
            'name'              => "doctor",
            'email'             => "doctor@gmail.com",
            'password'          => bcrypt('123456')
        ]);
        $userDoctorDefault->attachRole($roleDoctor);

        # PACIENTE
        $userPatientDefault = factory(User::class)->create([
            'name'              => "patient",
            'email'             => "patient@gmail.com",
            'password'          => bcrypt('123456')
        ]);

        $userPatientDefault->attachRole($rolePatient);


       # USUARIOS CREADOS DINAMICAMENTE
       
       # DOCTORES
        factory(User::class, 4)
            ->create()
            ->each(function ($doctor) use ($roleDoctor) {
                $doctor->attachRole($roleDoctor);
            });

        # PACIENTES
        factory(User::class, 50)
            ->create()
            ->each(function ($patient) use ($rolePatient) {
                $patient->attachRole($rolePatient);
            });
    }
}
