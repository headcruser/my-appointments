<?php

use App\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = Permission::DEFAULT_PERMISSIONS;

        foreach ($permissions as $perms) {
            Permission::firstOrCreate([
                'name'         => $perms,
                'display_name' => camel_case($perms),
                'description'  => 'description ' . $perms
            ]);
        }
    }
}
