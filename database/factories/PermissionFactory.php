<?php

use App\Permission;
use Faker\Generator as Faker;

$factory->define(Permission::class, function (Faker $faker) {
    $defaultPermission = $faker->randomElement(Permission::DEFAULT_PERMISSIONS);

    return [
        Permission::NAME          => $defaultPermission,
        Permission::DISPLAY_NAME  => ucfirst($$defaultPermission),
        Permission::DESCRIPTION   => $faker->realText(30, 2)
    ];
});
