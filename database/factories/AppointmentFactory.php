<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Appointment;
use App\Speciality;
use App\User;

use Faker\Generator as Faker;

$factory->define(Appointment::class, function (Faker $faker) {

    $date = $faker->dateTimeBetween('-1 years', 'now');
    $scheduled_date = $date->format('Y-m-d');
    $scheduled_time = $date->format('H:i:s');

    $types = ['Consulta','Examen','Operacion'];
    $status = ['Atendida','Cancelada'];

    return [
        'description'       => $faker->sentence(5),
        'specialty_id'      => Speciality::inRandomOrder()->first()->id,
        'doctor_id'         => User::doctors()->inRandomOrder()->first()->id,
        'patient_id'        => User::patients()->inRandomOrder()->first()->id,
        'scheduled_date'    => $scheduled_date,
        'scheduled_time'    => $scheduled_time,
        'type'              => $faker->randomElement($types),
        'status'            => $faker->randomElement($status)
    ];
});
