<?php

use App\Speciality;
use Faker\Generator as Faker;

$factory->define(Speciality::class, function (Faker $faker) {
    return [
        Speciality::NAME        => $faker->name,
        Speciality::DESCRIPTION => $faker->realText(30, 2),
    ];
});
