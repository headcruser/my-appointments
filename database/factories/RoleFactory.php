<?php

use App\Role;
use Faker\Generator as Faker;

$factory->define(Role::class, function (Faker $faker) {

    $defaultRole = $faker->randomElement(Role::DEFAULT_ROLES);

    return [
        Role::NAME          => $defaultRole,
        Role::DISPLAY_NAME  => ucfirst($$defaultRole),
        Role::DESCRIPTION   => $faker->realText(30, 2)
    ];
});
