@extends('layouts.panel')

@section('title','Reporte Medicos activos')
@section('content')
    <div class="card">
        <div class="card-header bg-transparent">
            <div class="row align-items-center">
            <div class="col">
                <h6 class="text-uppercase text-muted ls-1 mb-1">Reporte</h6>
                <h5 class="h3 mb-0">Medicos más activos</h5>
            </div>
            </div>
        </div>
        <div class="card-body">
            <div class="input-daterange datepicker row align-items-center" data-date-format="yyyy-mm-dd">
                <div class="col">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control" placeholder="Fecha de incio" id="startDate" 
                                type="text" 
                                value="{{ $start }}">
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control" placeholder="Fecha fin" id="endDate"
                                type="text" 
                                value="{{ $end }}">
                        </div>
                    </div>
                </div>
            </div>

            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>   
        </div>
    </div>
@endsection

@section('scripts') 
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    
    <script type="text/javascript">
        const chart = Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Medicos más activos'
            },
            xAxis: {
                categories: [],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                text: 'Citas atentidas'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                pointPadding: 0.2,
                borderWidth: 0
                }
            },
            series: []
            }
            );
        
        // DOM
        const startDate = document.getElementById('startDate');
        const endDate = document.getElementById('endDate');

        // EVENTS
        $(startDate).change(fetchData);
        $(endDate).change(fetchData);

        // ACTIONS FOR EVENTS
        async function fetchData(){
            const route = '{{ route('appointment.data') }}'
            const data = {
                'start' : startDate.value,
                'end'   : endDate.value
            }

            try{
                var response = await axios.post(route,data);

                chart.xAxis[0].setCategories(response.data.categories)

                if(chart.series.length > 0){
                    chart.series[1].remove();
                    chart.series[0].remove();
                }
                
                chart.addSeries(response.data.series[0])
                chart.addSeries(response.data.series[1])

                console.log(response.data.date);
            }catch(e){
                console.log(e);
            }
        }



        $(document).ready(fetchData);
    </script>
@endsection
