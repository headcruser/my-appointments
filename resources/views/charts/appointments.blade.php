@extends('layouts.panel')

@section('title','Reporte Frecuencia de citas')
@section('content')
    <div class="card">
        <div class="card-header bg-transparent">
            <div class="row align-items-center">
            <div class="col">
                <h6 class="text-uppercase text-muted ls-1 mb-1">Reporte</h6>
                <h5 class="h3 mb-0">Frecuencia de citas</h5>
            </div>
            </div>
        </div>
        <div class="card-body">
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>   
        </div>
    </div>
@endsection

@section('scripts') 
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    
    <script type="text/javascript">
        Highcharts.chart('container', {
            chart: {
                type: 'line'
            },
            credits: {
                enabled: false
            },
            title: {
                text: 'Citas registradas mensualmente'
            },
            xAxis: {
                categories: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']
            },
            yAxis: {
                title: {
                text: 'Cantidad de citas'
                }
            },
            plotOptions: {
                line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
                }
            },
            series: [
                {
                    name: 'Citas medicas',
                    data:  @json($appointmentsByMounts)
                }, 
            ]
            });
    </script>
@endsection
