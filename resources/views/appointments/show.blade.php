@extends('layouts.panel')

@section('title','Mi cita #'.$appointment->id)

@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <h3 class="mb-0"> Mi cita # {{ $appointment->id }} </h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <ul>
                <li><strong>Fecha:</strong> {{ $appointment->scheduled_date }}</li>
                <li><strong>Hora:</strong>  {{ $appointment->scheduled_time }}</li>
                <li><strong>Estado:</strong> {{ $appointment->status }}</li>

                @role(['admin', 'doctor'])
                    <li><strong>Paciente:</strong> {{ $appointment->patient->name }}</li>
                @endrole

                @role(['admin','patient'])
                     <li><strong>Médico:</strong> {{ $appointment->doctor->name }}</li>
                @endrole

               
                <li><strong>Especialidad:</strong> {{ $appointment->specialty->name }}</li>
                <li><strong>Tipo:</strong> {{ $appointment->type }}</li>
                <li><strong>Estado:</strong> 
                    <span class="badge {{ ($appointment->status == 'Cancelada')?'badge-danger':'badge-success' }}">{{ $appointment->status }}</span>
                </li>
            </ul>

            @if($appointment->status == 'Cancelada')
                <div class="alert alert-warning">
                    <p>Acerca de la cancelación</p>
                    @if($appointment->cancellation)
                        <li>
                            <strong>Motivo de la cancelacion</strong>
                            {{ $appointment->cancellation->justification }}
                        </li>
                        <li>
                            <strong>Fecha Cancelacion</strong>
                            {{ $appointment->cancellation->created_at }}
                        </li>
                        <li>
                            <strong>¿Quien Cancelo la cita?</strong>
                            @if(auth()->id() == $appointment->cancellation->id)
                            Tú
                            @else
                                {{ $appointment->cancellation->cancelledBy->name }}
                            @endif
                        </li>
                    @else
                        <li>Esta cita fue cancelada antes de su confirmación</li>
                    @endif
                </div>
            @endif

            <a href="{{ route('appointments.index') }}" class="btn btn-primary">Volver </a>

        </div>
    </div>


    <div class="modal fade" id="modal_cancel-appointment" tabindex="-1" role="dialog" aria-labelledby="modal_cancel-appointmentLabel" aria-hidden="true" style="display: none;">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content modal-lg">
            <div class="modal-header">
                <h5 class="modal-title" id="modal_cancel-appointment_title">Cancelar cita</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="ni ni-fat-remove"></i></span>
                </button>
            </div>
            <form id="form_cancel-appointment"  autocomplete="off" novalidate>
                <div class="modal-body">

                    <div class="form-group ">
                        <label class="form-control-label" for="justification">Por favor cuentanos el motivo de la cancelación</label>
                        <textarea name="justification" id="justification" cols="30" rows="3" class="form-control"></textarea>
                        <div class="invalid-feedback"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
                </div>
            </form>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        const APP_URL = "{{ route('home') }}"
    </script>
@endsection
