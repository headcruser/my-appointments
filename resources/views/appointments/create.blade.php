@extends('layouts.panel')

@section('title','Reservar Cita')
@section('content')

<div class="card shadow">
    <div class="card-header border-0">
        <div class="row align-items-center">
            <div class="col-md-5">
                <h3 class="modal-title"> Registrar nueva Cita </h3>
            </div>
            <div class="col-md-7 page-action text-right">
                <button id="button_create" type="button"
                    class="btn btn-primary btn-sm"> <i class="ni ni-single-02"></i>  Nuevo Paciente</button>
            </div>
        </div>
    </div>
    <div class="card-body">
        @if ($errors->any())
            <div class="alert alert-danger" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(session('notification'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('notification') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif

         <form action="{{ route('appointments.store') }}" method="POST"  autocomplete="off" id="form_appointment">
             @csrf
            <div class="modal-body">

                <div class="form-group">
                    <label class="form-control-label" for="description">Descripción</label>
                    <input name="description" type="text" class="form-control" placeholder="Describe Brevemente la consulta" value="{{ old('description') }}" required>
                </div>

                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label" for="specialty_id">Especialidad</label>

                            {!! Form::select('specialty_id',$specialties,old('specialty_id'), [
                                'class' => 'form-control',
                                'id'    => 'specialty_id',
                                ])
                            !!}

                            <div class="invalid-feedback"></div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group ">
                            <label class="form-control-label" for="doctor_id" required >Médicos</label>

                            {!! Form::select('doctor_id',$doctors, old('doctor_id'), [
                                'class' => 'form-control',
                                'id'    => 'doctor_id',
                                ])
                            !!}

                            <div class="invalid-feedback"></div>
                        </div>
                    </div>

                </div>


                <div class="form-group ">
                    <div class="form-group">
                         <label class="form-control-label" for="date">Fecha</label>
                        <div class="input-group input-group-alternative">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                            </div>
                            <input class="form-control datepicker" id="date"
                                name="scheduled_date"
                                type="text"
                                placeholder="Seleccionar Fecha"
                                data-date-format="yyyy-mm-dd"
                                data-date-start-date="{{ date('Y-m-d') }}"
                                data-date-end-date="+30d"
                                value="{{ old('scheduled_date',date('Y-m-d') ) }}">
                        </div>
                    </div>
                    <div class="invalid-feedback"></div>
                </div>
                 <div class="form-group">
                    <label for="address">Hora de atención</label>
                    <div id="hours_container">
                        @if($intervals)
                            @foreach ($intervals['morning'] as $idRadio => $interval)
                                <div class="custom-control custom-radio mb-3">
                                    <input name="scheduled_time" class="custom-control-input" id="morning_{{ $idRadio }}" type="radio" value="{{ $interval['start'] }}" required>
                                    <label class="custom-control-label" for="morning_{{ $idRadio }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
                                </div>
                            @endforeach

                            @foreach ($intervals['afternoon'] as $idRadio => $interval)
                                <div class="custom-control custom-radio mb-3">
                                    <input name="scheduled_time" class="custom-control-input" id="afternoon_{{ $idRadio }}" type="radio" value="{{ $interval['start'] }}" required>
                                    <label class="custom-control-label" for="afternoon_{{ $idRadio }}">{{ $interval['start'] }} - {{ $interval['end'] }}</label>
                                </div>
                            @endforeach
                        @else
                            <div class="alert alert-info" role="alert">
                                Seleccina un medico y una fecha para ver sus horas Disponibles
                            </div>
                        @endif
                    </div>
                </div>

                <div class="form-group ">
                    <label for="address">Tipo de consulta</label>
                    <div class="custom-control custom-radio mb-3">
                        <input name="type" class="custom-control-input" id="type1" type="radio" value="Consulta" @if( old('type','Consulta') == 'Consulta' ) checked @endif>
                        <label class="custom-control-label" for="type1">Consulta</label>
                    </div>
                    <div class="custom-control custom-radio mb-3">
                        <input name="type" class="custom-control-input" id="type2" type="radio" value="Examen"  @if( old('type') == 'Examen') ) checked @endif>
                        <label class="custom-control-label" for="type2">Examen</label>
                    </div>
                    <div class="custom-control custom-radio mb-3">
                        <input name="type" class="custom-control-input" id="type3" type="radio" value="Operación" @if( old('type') == 'Operación') ) checked @endif>
                        <label class="custom-control-label" for="type3">Operaciòn</label>
                    </div>

                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">

        class SelectComponent
        {
            constructor(selectId = '')
            {
                let type = typeof selectId;
                switch(type) {
                    case 'string':
                        if (selectId.length === 0) {
                            throw "No has escrito ninguna Propiedad"
                        }
                        this.el = document.getElementById(selectId);
                        if (!this.el) {
                            throw "El elemento no esta definido"
                        }
                    break;
                    case 'object':
                        if (!selectId instanceof HTMLElement) {
                            throw 'No es un elemento HTML';
                        }
                        this.el = selectId
                    break;
                    default:
                        throw "Debes definir un string | Objeto para iniciar el componente"
                }
            }
            /**
                * Establece una opcion por defecto
                *
                * @param {String} message
            */
            defaultOption(message){
                this.addOption(message,'',{selected: ''})
                return this;
            }
            addOptions(collection)
            {
                collection.forEach(element => {
                    var [text,value,attrs] = Object.values(element);
                    this.addOption(text, value,attrs);
                });
                return this;
            }
                /**
                * Agrega una opcion al Componente select
                *
                * @param {String} text Texto de la opcion
                * @param {String} value Valor de la opcion
                * @param {Array} attrs Attibutos adicionales para la opción
            */
            addOption(text = '', value = '' , attrs = [])
            {
                let option = document.createElement("option");
                option.text = text;
                option.value = value;
                    for (let attr of Object.keys(attrs)) {
                    option.setAttribute(attr, attrs[attr]);
                }
                this.el.add(option, '');
                return this;
            }
            /**
                * Limpia los elementos del componente
                *
                * @param {HtmlElement} select
            */
            clearOptions() {
                let options = Array.from(this.el.options);
                options.forEach(option => option.remove() );
                return this;
            }
            isEmpty(){
                return this.el.options.length === 0
            }
            getValue (){
                return this.el.value;
            }
            getTextOption(value = 0) {
                let option = this.getOption(value)
                if(!option) return null;
                return option.innerText;
            }
            getOption(value = 0){
                return this.el.querySelector(`option[value="${value}"]`)
            }
            setValue(value){
                this.el.value = value;
                return this;
            }
        }

        (function(){
            const select_specialties = document.getElementById('specialty_id');
            const doctorSelectComponent = new SelectComponent('doctor_id');
            const datePicker = document.getElementById('date')
            const hoursContainer = document.getElementById('hours_container')
            const urlBase = "{{ url('/') }}/api/v1";

            var idRadio = 0;



            select_specialties.addEventListener('change',handleSpecialties);
            doctorSelectComponent.el.addEventListener('change',handleDoctors);
            $(datePicker).change(handleDate)

            async function handleSpecialties(event)
            {
                let idSpecialty = event.target.value;

                if(!idSpecialty){
                    doctorSelectComponent.clearOptions()
                        .defaultOption('Selecciona antes una especialidad')

                    return;
                }

                let api = `${urlBase}/specialties/${idSpecialty}/doctors`;

                try {
                    let doctors = await axios.get(api)

                    doctorSelectComponent.clearOptions()
                        .addOptions(doctors.data);

                    loadHours();

                }catch(e){
                    Swal.fire({
                        type: 'info',
                        title: 'Error',
                        text: error,
                    });
                }
            }

            function handleDoctors(event){
                console.log('change doctors');
            }

            function handleDate(event){
                loadHours();
            }

            async function loadHours()
            {
                let hours = {};
                try{
                    let selectedDay = datePicker.value;
                    let idDoctor = doctorSelectComponent.el.value
                    let response = await axios.get(`${urlBase}/schedule/hours?date=${selectedDay}&doctor_id=${idDoctor}`);

                    displayHours(response.data);
                }catch(e){
                    console.error(e)
                }
            }

            function displayHours(data)
            {
                if(!data.morning && !data.afternoon || data.morning.length == 0 && data.afternoon.length == 0) {
                    hoursContainer.innerHTML = `
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            Lo sentimos, No hay horarios diponibles:

                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>`;

                    return;
                }

                let htmlHours = ``;
                idRadio = 0;

                if(data.morning){
                    let morning_intervals = data.morning;
                    morning_intervals.forEach(interval => {
                        htmlHours+= getRadioIntervalHTML(interval)
                    })
                }

                if(data.afternoon){
                    let afternoon_intervals = data.afternoon;
                    afternoon_intervals.forEach(interval => {
                        htmlHours+= getRadioIntervalHTML(interval)
                    })
                }

                hoursContainer.innerHTML = htmlHours;
            }

            function getRadioIntervalHTML(interval){
                let text  = `${interval.start} - ${interval.end}`
                idRadio++;

                return `
                <div class="custom-control custom-radio mb-3">
                    <input name="scheduled_time" class="custom-control-input" id="interval_${idRadio}" type="radio" value="${interval.start}" required>
                    <label class="custom-control-label" for="interval_${idRadio}">${text}</label>
                </div>`
            }
        })();


    </script>
@endsection
