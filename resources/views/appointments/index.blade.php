@extends('layouts.panel')

@section('title','Mis Citas')
@section('content')

    <div class="card">
        <div class="card-header">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <h3 class="mb-0"> Mis Citas </h3>
                </div>
            </div>
        </div>
        <div class="card-body">
            <ul class="nav nav-pills" id="tab-appointments" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="confirm-appointment-tab" 
                        data-toggle="tab" 
                        href="#confirm-appointment" 
                        role="tab" 
                        aria-controls="confirm-appointment" 
                        aria-selected="true">Mis proximas Citas</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="pending-appointment-tab"
                        data-toggle="tab" 
                        href="#pending-appointment" 
                        role="tab" 
                        aria-controls="pending-appointment"
                        aria-selected="false">Citas por confirmar</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="history-appointment-tab" 
                        data-toggle="tab" 
                        href="#history-appointment" 
                        role="tab" 
                        aria-controls="history-appointment" 
                        aria-selected="false">Historial de citas</a>
                </li>
            </ul>
        
        </div>
    </div>

    {{--  seccion de citas  --}}
    <div class="tab-content" id="contentTabAppointments">

        <div class="tab-pane fade show active" id="confirm-appointment" role="tabpanel" aria-labelledby="confirm-appointment-tab">
            <div class="table-responsive" id="tb-container-confirmed-appointments">
            @include('appointments.tables._confirmed')
            </div>
        </div>
        <div class="tab-pane fade" id="pending-appointment" role="tabpanel" aria-labelledby="pending-appointment-tab">
            <div class="table-responsive" id="tb-container-pending-appointments">
                @include('appointments.tables._pending')
            </div>
        </div>
        <div class="tab-pane fade" id="history-appointment" role="tabpanel" aria-labelledby="history-appointment-tab">
            <div class="table-responsive" id="tb-container-history-appointments">
                @include('appointments.tables._history')
            </div>
        </div>
    </div>


    {{-- modal_cancel-appointment  --}}
    @include('appointments.cancel')

@endsection

@section('scripts')
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script type="text/javascript">
        

        const confirmDialog = async function(message , type ) {
             let answer = await  Swal.fire({
                title: message,
                type: type,
                customClass: {
                    icon: 'swal2-arabic-question-mark'
                },
                confirmButtonText: 'Aceptar',
                cancelButtonText: 'Cancelar',
                showCancelButton: true,
                showCloseButton: true
            })

            return answer;
        };

        const dialog = function(type,title,message){
            Swal.fire({
                type: type,
                title: title,
                text: message,
            })
        };

        const serialize = function(form) {
            return Array.from(
                new FormData(form),
                e => e.map(encodeURIComponent).join('=')
            ).join('&');
        }

        const APP_URL = "{{ route('home') }}";
        const appDom = {
            'confirm': {
                'container': document.getElementById('tb-container-confirmed-appointments'),
            },
            'pending':{
                'container': document.getElementById('tb-container-pending-appointments')
            },
            'history':{
                'container': document.getElementById('tb-container-history-appointments')
            },
            'modal':{
                'cancel': document.getElementById('modal_cancel-appointment')
            }
        }


        const confirmAppointment = (function() {
            const tbContainerPending = appDom.pending.container;
            const tbContainerConfirmed = appDom.confirm.container;

            const selectorAction = `tbody tr td button[data-action="confirm"]`;

            tbContainerPending.addEventListener('click', handlerConfirm)

            async function handlerConfirm(e)
            {
                if (! e.target.closest(selectorAction) ) {
                    return;
                }

                let answer = await confirmDialog('Deseas Confirmar la cita','warning');

                if(!answer.value){
                    return
                }

                let idAppointment = e.target.parentElement.parentElement.dataset.id;
                
                let url = `${APP_URL}/appointments/${idAppointment}/confirm`;

                try {
                    let response = await axios.post(url)

                    tbContainerConfirmed.innerHTML = response.data.templateConfirmed;
                    tbContainerPending.innerHTML = response.data.templatePending

                    dialog('success','Éxito',response.data.message)
                }catch(e) {
                    dialog('info','Ups, Hubo un error', e)
                }
            }
        })();

        const cancelConfirmedApppintment = (function(){
            // DOM ELEMENTS
            const tbContainerConfirmed = appDom.confirm.container,
                  tbContainerHistory = appDom.history.container,
                  tbContainerPending = appDom.pending.container,
                  
                  modalCancelAppointment = appDom.modal.cancel,
                  formCancelAppointment = document.getElementById('form_cancel-appointment'),

                  cancelPatientAction = `tbody tr td button[data-action="cancel"]`;
           
            // EVENTS
            tbContainerConfirmed.addEventListener('click',handlerCancelAppointment)
            formCancelAppointment.addEventListener('submit',handleSaveCancel)

            // ACTIONS FOR EVENTS
            function handlerCancelAppointment(e)
            {
                if (! e.target.closest(cancelPatientAction) ) {
                    return;
                }

                let idAppointment = e.target.parentElement.parentElement.dataset.id;
                modalCancelAppointment.dataset.id = idAppointment
                
                $(modalCancelAppointment).modal('show');     // visibility ? 'show':'hide'
            }

            async function handleSaveCancel(e)
            {
                e.preventDefault();
                let idAppointment = modalCancelAppointment.dataset.id;

                let url = `${APP_URL}/appointments/${idAppointment}/cancel`;
                let data = serialize(formCancelAppointment)

                try {
                    let response = await axios.post(url,data)

                    tbContainerConfirmed.innerHTML = response.data.templateConfirmed;
                    tbContainerHistory.innerHTML = response.data.templateHistory;
                    tbContainerPending.innerHTML = response.data.templatePending;

                    $(modalCancelAppointment).modal('hide');

                    dialog('success','Éxito',response.data.message)
                        
                }catch(e){
                    dialog('info','Ups, Hubo un error', e)
                }
            }
        })()

        const cancelAppointment = (function(){
            // DOM
            const tbContainerPending = appDom.pending.container;
            const tbContainerHistory = appDom.pending.history;

            const modalCancelAppointment = appDom.modal.cancel

            const cancelPatient = `tbody tr td button[data-action="cancel"]`;
            const cancelJustification = `tbody tr td button[data-action="canceljustification"]`;

            // REGISTER EVENTS 
            tbContainerPending.addEventListener('click', handlerCancelAppointment);
            tbContainerPending.addEventListener('click', handlerCancelJustificationModal);

            // ACTIONS FOR EVENTS 
            async function handlerCancelAppointment(e) 
            {
                if ( !e.target.closest(cancelPatient) ) {
                    return;
                }

                let button = event.target.parentElement,
                    tr = button.parentElement,
                    idAppointment = tr.dataset.id,
                    url = `${APP_URL}/appointments/${idAppointment}/cancel`
                
                try 
                { 
                    let answer = await  confirmDialog('¿Deseas cancelar la cita?','warning');

                    if(!answer.value) return;
                    
                    let response = await axios.post(url)

                    tbContainerPending.innerHTML = response.data.templatePending;
                    tbContainerHistory.innerHTML = response.data.templateHistory
                    
                    dialog('success','exito', response.data.message)
                } catch(e) { 
                    dialog('info','Ups, Hubo un error', e)
                }
            }

            function handlerCancelJustificationModal(e)
            {
                if ( !e.target.closest(cancelJustification) ) {
                    return;
                }

                let idAppointment = e.target.parentElement.parentElement.dataset.id;
                modalCancelAppointment.dataset.id = idAppointment

                $(modalCancelAppointment).modal('show');  // visibility ? 'show':'hide'
            }
        })();
    </script>
@endsection
