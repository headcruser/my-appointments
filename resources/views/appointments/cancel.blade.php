<div class="modal fade" id="modal_cancel-appointment" tabindex="-1" role="dialog" aria-labelledby="modal_cancel-appointmentLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content modal-lg">
        <div class="modal-header">
            <h5 class="modal-title" id="modal_cancel-appointment_title">Cancelar cita</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true"><i class="ni ni-fat-remove"></i></span>
            </button>
        </div>
        
        {{--  TEXTO DE REFERENCIA PARA EL ROL QUE HA INICIADO SESION   --}}
        <form id="form_cancel-appointment"  autocomplete="off" novalidate>
            <div class="modal-body">
                <div class="form-group ">
                    <label class="form-control-label" for="justification">Por favor cuentanos el motivo de la cancelación</label>
                    <textarea name="justification" id="justification" cols="30" rows="3" class="form-control"></textarea>
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
            </div>
        </form>
        </div>
    </div>
</div>