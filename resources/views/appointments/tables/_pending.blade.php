<table class="table table-bordered table-striped table-hover" id="tb_pending_appointment">
    <thead>
        <tr>
            <th>Id</th>
            <th>Descripcion</th>
            <th>Especialidad</th>
            @role('doctor')
                <th>Paciente</th>
            @endrole
            @role('patient')
                <th>Doctor</th>
            @endrole
            <th>Fecha</th>
            <th>Hora</th>
            <th>Tipo Cita</th>
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($pendingCollection as $item)
            <tr data-id="{{$item->id}}" data-object="{{ $item }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->description }}</td>
                <td>{{ $item->specialty->name }}</td>

                @role('doctor')
                    <td>{{ $item->patient->name }}</td>
                @endrole

                @role('patient')
                    <td>{{ $item->doctor->name }}</td>
                @endrole

                <td>{{ $item->scheduled_date }}</td>
                <td>{{ $item->scheduled_time_12 }}</td>
                <td>{{ $item->type }}</td>
                <td class="text-center">
                    @role('admin')
                        <a href="{{ route('appointments.show',$item) }}" title="Ver Cita" class="btn btn-primary btn-sm ni ni-archive-2"></a>
                    @endrole

                    @role(['doctor','admin'])
                        <button class="btn btn-success btn-sm btn-icon ni ni-check-bold" data-action="confirm" title="Confirmar cita"></button>
                        <button class="btn btn-danger btn-sm btn-icon far fa-trash-alt"
                            data-action="canceljustification"
                            data-role='doctor-admin'
                            title="Cancelar cita">
                        </button>
                    @endrole

                    @role(['patient'])
                        <button class="btn btn-danger btn-sm btn-icon far fa-trash-alt"
                            data-action="cancel"
                            data-role='patient'
                            title="Cancelar cita"></button>
                    @endrole

                </td>
            </tr>
        @empty
            <tr class="no-records-found">
                <td class="text-center" colspan="{{ 8 }}">No se encontraron resultados</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="card-body">
    {{ $pendingCollection->links() }}
</div>
