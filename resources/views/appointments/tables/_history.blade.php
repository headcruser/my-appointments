<table class="table table-bordered table-striped table-hover" id="tb_history_appointment">
    <thead>
        <tr>
            <th>Especialidad</th>
            <th>Fecha</th>
            <th>Hora</th>
            <th class="text-center">Estado</th>
            <th class="text-center">Opciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($historyCollection as $item)
            <tr data-id="{{$item->id}}" data-object="{{ $item }}">
                <td>{{ $item->specialty->name }}</td>
                <td>{{ $item->scheduled_date }}</td>
                <td>{{ $item->scheduled_time_12 }}</td>
                <td class="text-center"> <span class="badge badge-primary">{{ $item->status }}</span> </td>
                <td class="text-center"> <a href="{{ route('appointments.show',$item)}}" class="btn btn-primary btn-sm"> Ver</a></td>
            </tr>
        @empty
            <tr class="no-records-found">
                <td class="text-center" colspan="{{ 9 }}">No se encontraron resultados</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="card-body">
    {{ $historyCollection ->links() }}
</div>
