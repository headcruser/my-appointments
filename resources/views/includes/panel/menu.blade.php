<!-- Navigation -->

@role('admin')
<h6 class="navbar-heading text-muted">Administración</h6>
<ul class="navbar-nav">
  <li class="nav-item">
    <a class="nav-link {{ routeIs('users.index','active bg-gradient-lighter') }}"  href="{{ route('users.index') }}">
      <i class="ni ni-circle-08 text-blue"></i> <span class="nav-link-text">Usuarios</span>
    </a>
  </li>
   <li class="nav-item">
    <a class="nav-link {{ routeIs('roles.index','active bg-gradient-lighter') }}"  href="{{ route('roles.index') }}">
      <i class="ni ni-badge text-info"></i> <span class="nav-link-text">Roles</span>
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link {{ routeIs('permissions.index','active bg-gradient-lighter') }}" href="{{ route('permissions.index') }}">
        <i class="ni ni-settings-gear-65 text-danger"></i> <span class="nav-link-text">Permisos</span>
    </a>
  </li>
</ul>
<hr class="my-3">
@endrole

<h6 class="navbar-heading text-muted">
    @role('admin') Gestionar datos @endrole
    @role('doctor') Menú @endrole
</h6>
<ul class="navbar-nav">

    <li class="nav-item">
        <a class="nav-link {{ routeIs('profile','active bg-gradient-lighter') }}"  href="{{ route('profile') }}">
            <i class="ni ni-circle-08 text-blue"></i> <span class="nav-link-text">Perfil</span>
        </a>
    </li>

    @role('admin')
        <li class="nav-item">
            <a class="nav-link {{ routeIs('home','active bg-gradient-lighter') }}" href="{{ route('home') }}">
                <i class="ni ni-tv-2 text-danger"></i> <span class="nav-link-text">Dashboard</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ routeIs('specialties.index','active bg-gradient-lighter') }}"  href="{{ route('specialties.index') }}">
                <i class="ni ni-planet text-blue"></i> <span class="nav-link-text">Especialidades</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ routeIs('doctors.index','active bg-gradient-lighter') }}"  href="{{ route('doctors.index') }}">
                <i class="ni ni-single-02 text-red"></i> <span class="nav-link-text">Médicos</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ routeIs('patients.index','active bg-gradient-lighter') }}" href="{{ route('patients.index') }}">
                <i class="ni ni-satisfied text-info"></i> <span class="nav-link-text">Pacientes</span>
            </a>
        </li>
         <li class="nav-item">
            <a class="nav-link {{ routeIs('appointments.index','active bg-gradient-lighter') }}" href="{{ route('appointments.index') }}">
                <i class="ni ni-time-alarm text-danger"></i> <span class="nav-link-text">Citas Médicas</span>
            </a>
        </li>
    @endrole

    @role('doctor')
        <li class="nav-item">
            <a class="nav-link {{ routeIs('schedule','active bg-gradient-lighter') }}"  href="{{ route('schedule') }}">
                <i class="ni ni-calendar-grid-58 text-red"></i> <span class="nav-link-text">Gestionar Horarios</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link {{ routeIs('appointments.index','active bg-gradient-lighter') }}"  href="{{ route('appointments.index') }}">
                <i class="ni ni-time-alarm text-info"></i> <span class="nav-link-text">Mis Citas</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <i class="ni ni-satisfied text-orange"></i> <span class="nav-link-text">Mis Pacientes</span>
            </a>
        </li>
    @endrole

    @role('patient')
        <li class="nav-item">
            <a class="nav-link {{ routeIs('appointments.create','active bg-gradient-lighter') }}" href="{{ route('appointments.create') }}">
                <i class="ni ni-send text-info"></i> <span class="nav-link-text">Reservar Cita</span>
            </a>
        </li>

        <li class="nav-item">
            <a class="nav-link {{ routeIs('appointments.index','active bg-gradient-lighter') }}" href="{{ route('appointments.index') }}">
                <i class="ni ni-time-alarm text-danger"></i> <span class="nav-link-text">Mis citas</span>
            </a>
        </li>
    @endrole

  <li class="nav-item">
    <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('formLogout').submit();">
      <i class="ni ni-key-25"></i> Cerrar sesión
    </a>
    <form action="{{ route('logout') }}" method="POST" style="display: none;" id="formLogout">
      @csrf
    </form>
  </li>

</ul>

@role('admin')
<!-- Divider -->
<hr class="my-3">
<!-- Heading -->
<h6 class="navbar-heading text-muted">Reportes</h6>
<!-- Navigation -->
<ul class="navbar-nav mb-md-3">
  <li class="nav-item">
    <a class="nav-link" href="{{ route('appointment.line') }}">
      <i class="ni ni-sound-wave text-yellow"></i> Frecuencia de citas
    </a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="{{ route('appointment.column') }}">
      <i class="ni ni-spaceship text-orange"></i> Médicos más activos
    </a>
  </li>
</ul>
@endrole
