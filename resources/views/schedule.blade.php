@extends('layouts.panel')

@section('title','Gestion de Horarios')

@section('content')
    <form action="" method="POST">
        @csrf
        <div class="card shadow">
            <div class="card-header border-0">
                <div class="row align-items-center">
                    <div class="col-md-5">
                        <h3 class="modal-title">Gestion de horarios</h3>
                    </div>
                    <div class="col-md-7 page-action text-right">
                        <button id="button_create" type="submit"
                            class="btn btn-primary btn-sm"> <i class="ni ni-calendar-grid-58"></i>  Guardar Cambios</button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                @if(session('notification'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{ session('notification') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif

                @if(session('errors'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        Los cambios se han guardado, pero debes tener en cuenta que : 
                        <ul>
                            @foreach (session('errors') as $error )
                               <li>{{ $error }}</li> 
                            @endforeach
                        </ul>
                        
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            </div>
            <div class="table-responsive" id="table_container">
                <table class="table align-items-center table-flush" id="table_schedule">
                    <thead class="thead-lightd">
                        <tr>
                            <th>Dia</th>
                            <th>Activo</th>
                            <th>Turno Mañana</th>
                            <th>Turno Tarde</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($workDays as $key=>$workDay)
                        <tr>
                            <th>{{ $days[$key] }}</th>
                            <td>
                                <label class="custom-toggle">
                                    <input name="active[]" type="checkbox" @if($workDay->active) checked @endif value="{{ $key }}">
                                    <span class="custom-toggle-slider rounded-circle"></span>
                                </label>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="form-control" name="morning_start[]">
                                            @for ($i = 5 ; $i <= 11 ; $i++)
                                                <option value="{{ ($i<10 ? '0':'') . $i }}:00"
                                                    @if ($i.':00 AM'  == $workDay->morning_start) selected @endif >
                                                    {{ $i }}:00 AM
                                                </option>
                                                <option value="{{ ($i<10 ? '0':'') . $i }}:30"
                                                    @if ($i.':00 AM'  == $workDay->morning_start) selected @endif >
                                                    {{ $i }}:30 AM
                                                </option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="form-control" name="morning_end[]">
                                            @for ($i = 5 ; $i <= 11 ; $i++)
                                                <option value="{{ ($i<10 ? '0':'') . $i }}:00"
                                                    @if ($i.':00 AM'  == $workDay->morning_end) selected @endif >
                                                    {{ $i }}:00 AM
                                                </option>
                                                <option value="{{ ($i<10 ? '0':'') . $i }}:30"
                                                    @if ($i.':00 AM'  == $workDay->morning_end) selected @endif >
                                                    {{ $i }}:30 AM
                                                </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <select class="form-control" name="afternoon_start[]">
                                            @for ($i = 1 ; $i <= 11 ; $i++)
                                                <option value="{{ $i + 12 }}:00"
                                                    @if ($i.':00 PM'  == $workDay->afternoon_start) selected @endif>
                                                    {{ $i }}:00 PM
                                                </option>
                                                <option value="{{ $i + 12 }}:00"
                                                    @if ($i.':30 PM'  == $workDay->afternoon_start) selected @endif>
                                                    {{ $i }}:30 PM
                                                </option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col">
                                        <select class="form-control" name="afternoon_end[]">
                                            @for ($i = 1 ; $i <= 11 ; $i++)
                                                <option value="{{ $i + 12 }}:00"
                                                    @if ($i.':00 PM'  == $workDay->afternoon_end) selected @endif>
                                                    {{ $i }}:00 PM
                                                </option>
                                                <option value="{{ $i + 12 }}:30"
                                                    @if ($i.':30 PM'  == $workDay->afternoon_end) selected @endif>
                                                    {{ $i }}:30 PM
                                                </option>
                                            @endfor
                                        </select>
                                    </div>
                                </div>
                            </td>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </form>
@endsection
