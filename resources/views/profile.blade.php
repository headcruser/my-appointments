@extends('layouts/panel')

@section('title', 'Editar Perfil')

@section('content')
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">Editar Perfil </h3>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    @if(session('notification'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('notification') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif
                <form id="form-profile"  method="POST" action="{{ route('profile.update') }}" enctype='multipart/form-data'>
                    @csrf
                    <h6 class="heading-small text-muted mb-4">Perfil de usuario</h6>
                    <div class="pl-lg-4">
                        <div class="card-body p-0 m-0">
                            <a href="#!">
                                <img id="avatar-profile" src="{{ $authUser->getAvatarUrl() }}" class="rounded-circle img-center img-fluid shadow shadow-lg--hover"  style="width:140px; height:140px;">
                            </a>
                            <div class="pt-4 text-center">
                                <h5 class="h3 title">
                                <span class="d-block mb-1">{{ $authUser->name }}</span>
                                <small class="h4 font-weight-light text-muted">{{ $authUser->displayNameRole() }}</small>
                                </h5>
                            </div>
                        </div>
                        <div class="input-group">
                            <input type="file" class="form-control-file" id="photo" name="photo">
                        </div>
                    </div>
                    <hr class="my-4">

                    <h6 class="heading-small text-muted mb-4">Información de contacto</h6>
                    <div class="pl-lg-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="dni">DNI</label>
                                    <input id="dni" name="dni" class="form-control" placeholder="DNI" value="{{ $authUser->dni }}" type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="address">Dirección</label>
                                    <input type="text" name="address" id="address" class="form-control" placeholder="Dirección" value="{{ $authUser->address }}">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="form-control-label" for="phone">Telefono</label>
                                    <input type="text" name="phone" id="phone" class="form-control" placeholder="Country" value="{{ $authUser->phone }}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button class="btn btn-primary" type="submit">Actualizar Perfil</button>
                    </div>
                </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">
        const updateProfile = (function()
        {
            const formProfile = document.getElementById('form-profile'),
                  photoInput = document.getElementById('photo'),
                  avatarProfile = document.getElementById('avatar-profile'),
                  avatarNavbar = document.getElementById('avatar-navbar');

            formProfile.addEventListener('submit',handleSubmit)
            photoInput.addEventListener('change',handleChangeImage)

            async function handleSubmit(event){
                event.preventDefault();

                let form = event.target;

                updateProfile(form)
            }

            async function handleChangeImage(event)
            {
                let input = event.target,
                    fileList = input.files,
                    firstFile = fileList[0],
                    reader = new FileReader();

                if(!firstFile){
                    return;
                }

                if ( !firstFile.type.match('image.*') ) {
                    await Swal.fire('Atencion',"Solo debes ingresar imagenes",'warning')
                    input.value = null;
                    return;
                }

                reader.onload = function(readerEvent)
                {
                    if(!avatarProfile || !avatarNavbar)
                        return

                    avatarProfile.src = readerEvent.target.result;
                    avatarNavbar.src = readerEvent.target.result;
                }

                reader.readAsDataURL(firstFile);
            }

            async function updateProfile(form)
            {
                try
                {
                    let settings = { headers: { 'content-type': 'multipart/form-data' } },
                        dataForm = serialize(form),
                        response =  await axios.post(form.action, dataForm , settings);

                    if (response.status === 200) {
                        Swal.fire('Exito',response.data.message,'success')
                    }
                }catch(e) {
                    if( e.hasOwnProperty('response')){
                        Swal.fire('Error','Error al actualizar el perfil','error')
                        return;
                    }

                    Swal.fire('Error',e,'error')

                }
            }

            function serialize(form)
            {
                let formData = new FormData(form),
                    file = photoInput.files[0];

                if (file) {
                    formData.append('photo', file, file.name)
                }

               return formData;
            }

        })()
    </script>

@endsection
