@extends('layouts.panel')

@section('title','Ajustes')

@section('content')
    <div class="card card-frame">
        <div class="card-header">
            <h3 class="mb-0">Ajustes del sistema</h3>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-md-12">

                    @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <span class="alert-icon"><i class="ni ni-like-2"></i></span>
                            <span class="alert-text">{{ session('status') }}</span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                    @endif

                    <form method="post" action="{{ route('settings.store') }}" class="form-horizontal" role="form">
                        @csrf
                            @forelse(config('setting_fields') as $section => $fields)
                                <div class="card mb-4">
                                    <div class="card-header bg-primary text-white">
                                        <i class="{{ array_get($fields, 'icon', 'glyphicon glyphicon-flash') }}"></i>
                                        {{ $fields['title'] }}
                                    </div>

                                    <div class="card-body">
                                        <p class="text-muted">{{ $fields['desc'] }}</p>
                                    </div>

                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                @foreach($fields['elements'] as $field)
                                                    @includeIf('setting.fields.' . $field['type'] )
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                @empty
                                    <div class="card card-frame">
                                        <div class="card-header">
                                            No hay Ajustes disponibles
                                        </div>
                                    </div>
                                @endforelse
                                <!-- end panel for {{ $fields['title'] }} -->

                        <div class="row m-b-md">
                            <div class="col-md-12">
                                <button class="btn-primary btn">
                                     {{ __('Save Settings') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
