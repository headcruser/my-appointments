@extends('layouts.panel')

@section('title','Especialidades')

@section('content')
<div class="card shadow">
    <div class="card-header border-0">
      <div class="row align-items-center">
        <div class="col">
          <h3 class="mb-0">Especialidades</h3>
        </div>
        <div class="col text-right">
            <button id="button_create" type="button"
                    class="btn btn-primary btn-sm">
                Nueva Especialidad
            </button>
        </div>
      </div>
    </div>
    <div class="table-responsive" id="table_container">
        @include('specialties.partials._table')
    </div>
  </div>


  <div class="modal fade" id="modal_crud" tabindex="-1" role="dialog" aria-labelledby="modal_crudLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content modal-lg">
        <div class="modal-header">
          <h5 class="modal-title" id="modal_crud_title">Crear Especialidad</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <form id="form_modal"  autocomplete="off" novalidate>
            <div class="modal-body">

                <div class="form-group ">
                    <label class="form-control-label" for="name">Nombre de la especialidad</label>
                    <input class="form-control" type="text" name="name" >
                    <div class="invalid-feedback"></div>
                </div>
                <div class="form-group">
                    <label for="description">Descripción</label>
                    <textarea class="form-control" name="description" id="description" cols="30" rows="5"></textarea>
                    <div class="invalid-feedback"></div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                <button type="submit" class="btn btn-primary btn-sm">Guardar</button>
            </div>
        </form>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/sweet_alert.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>

    <script type="text/javascript">

        const dom = {
            create: {
                el: document.getElementById('button_create'),
                event: 'click'
            },
            edit:{
                event: 'click',
                selector: 'tbody tr td button[data-action="edit"]'
            },
            destroy:{
                event: 'click',
                selector: 'tbody tr td button[data-action="delete"]'
            },
            paginator: {
                selector: 'a.page-link',
                event: 'click',
            },
            table:{
                el: document.getElementById('table_model'),
                selector: 'tbody tr td i[data-action="edit"]',
                container: document.getElementById("table_container"),
            },
            modal: {
                el: document.getElementById('modal_crud'),
                titles:{
                    el: document.getElementById('modal_crud_title'),
                    create:'Crear Especialidad',
                    edit: 'Editar Especialidad',
                },
                form:{
                    el: document.getElementById('form_modal'),
                    submit: document.getElementById("form_modal").querySelector(`button[type="submit"]`),
                    reset: document.getElementById("form_modal").querySelector(`button[type="reset"]`),
                    event: 'submit',
                    fields: {}
                },
                url:'{{ $model_route }}'
            }
        };


        class CrudComponent
        {
            constructor(properties = {}) {

                this.timeWaitSearch = null;

                if (this._isEmptyProperties(properties)){
                    throw "No has definido ninguna propiedad"
                }

                this._create = properties.create;
                this._edit = properties.edit;
                this._destroy = properties.destroy;

                this._search = properties.search;
                this._paginator = properties.paginator;
                this._refresh = properties.refresh;
                this._table = properties.table;
                this._modal = properties.modal;
                this._form  = properties.modal.form;
                this._orderBy = properties.orderBy

                this._registerEvents();
            }

            /**
             * Evento que muestra el modal para crear un nuevo elemento
             *
             * @param {*} event
            */
            create(event)
            {
                this._updateTitleModal(this._modal.titles.create);
                this._updateTextSubmitButton('Crear');
                this._setAction('POST', this._modal.url)
                this._clearForm();
                this._displayModal();
            }

            /**
             * Evento que muestra el modal para editar un elemento.
             *
             * @param {Event} event
            */
            edit(event)
            {
                let object = this.readDataObject(event.target);

                this._updateTitleModal(this._modal.titles.edit);
                this._updateTextSubmitButton('Editar');
                this._setAction('PUT', `${this._modal.url}/${object.id}`)

                this._clearForm();

                this._readFormData(this._modal.form.el, object);

                this._displayModal();
            }

            /**
             * @param {HtmlElement} object
             * @param {Object} errors
            */
            destroy(event) {
                let self = this,
                    object = self.readDataObject(event.target),
                    url = `${self._modal.url}/${object.id}`;


                Swal.fire({
                    title: 'Deseas eliminar el registro',
                    type: 'error',
                    customClass: {
                        icon: 'swal2-arabic-question-mark'
                    },
                    confirmButtonText: 'Aceptar',
                    cancelButtonText: 'Cancelar',
                    showCancelButton: true,
                    showCloseButton: true
                }).then((result) => {
                    if (!result.value) return;

                        axios.delete(url)
                        .then(function(response){
                            try {
                                self.renderUI(response);

                                Swal.fire({
                                    type: 'success',
                                    title:'Éxito',
                                    text: response.data.message,
                                })

                            }catch(error) {
                                Swal.fire({
                                    type: 'info',
                                    title: 'Error código fuente',
                                    text: error,
                                });
                            }
                        })
                        .catch(function(error){
                            Swal.fire({
                                    type: 'info',
                                    title: 'error',
                                    text: error,
                            });
                        })
                });
            }

            /**
             * Evento que se encarga de enviar el formulario a travez de una petion ajax
             *
             * @param {Event} event
            */
            save(event)
            {
                event.preventDefault();

                let self = this,
                    form = self._modal.form.el,
                    data = self._serialize(form),
                    url = form.dataset.url,
                    method = form.dataset.method;

                axios({
                    method  : method,
                    url     : url,
                    data    : data
                })
                    .then( function (response){
                        try {
                            self.renderUI(response);
                            Swal.fire({
                                    type: 'success',
                                    title:'Éxito',
                                    text: response.data.message,
                            }).then(function(){
                                self._displayModal(false);
                            });
                        }catch(error) {
                            Swal.fire({
                                type: 'info',
                                title: 'Error código fuente',
                                text: error,
                            });
                        }
                    })
                    .catch( function(error) {
                        let data = error.response.data;

                        if(data.hasOwnProperty('errors')){
                            self._readFormErrors(form,data.errors);
                        }else{
                            Swal.fire({
                                type: 'danger',
                                title: 'Error Servidor',
                                text: data.message,
                            });
                        }
                    });
            }

             /**
             * Evento que permite navegar entre la informacion
             *
            */
            paginate(event) {
                event.preventDefault();
                this.refresh(event.target.getAttribute('href'));
            }

            /**
             * Actualiza la pantalla de elementos, segun la ruta asignada
             * como parametro
             *
             * @param {string} url
            */
            refresh(url)
            {
                let self = this;
                axios.get(url)
                    .then( function (response)  {
                        try {
                            self.renderUI(response);
                        }catch(error) {
                            Swal.fire({
                                    type: 'info',
                                    title: 'Error código fuente',
                                    text: 'error',
                            });
                        }
                    })
                    .catch(function(error){
                        Swal.fire(
                            'Error',
                            error.response.data,
                            'danger'
                        )
                    });
            }

            /**
             * Renderiza la  respuesta del servidor.
             *
             * @param {Event} response
            */
            renderUI(response)
            {
                switch (response.status)
                {
                    case 200:
                        if (!response.hasOwnProperty('data')) {

                            Swal.fire({
                                type: 'warning',
                                title:'Atención',
                                text: 'No se ha recibido información del servidor',
                            });
                            return;
                        }

                        if (!response.data.hasOwnProperty('template')) {

                            Swal.fire({
                                type: 'warning',
                                title:'Atención',
                                text: 'No se ha recibido el template a renderizar',
                            });
                            return;
                        }

                        this._renderTable(response.data.template);
                }
            }

            /**
             * Lee el atributo del elemento HTML especificado.
             * Si la propiedad no existe, regresa un objeto
             * vacio.
             *
             * @param {HTMLElement} element
            */
            readDataObject(element) {
                let td = element.parentElement,
                    tr = td.parentElement;

                return this.parseDataSetObject(tr);
            }

            /**
             * Convierte la informacion del atributo object y
             * lo convierte en un objeto.
             *
             * Si el elemento no contiene el atributo 'object', regresa
             * un objeto vacio.
             *
             * @param {HtmlElement} element
             * @return {Object}
            */
            parseDataSetObject(element)
            {
                let data = '{}';

                if (element.dataset.object) {
                    data = element.dataset.object;
                }

                return JSON.parse(data);
            }

            /**
             * Asigna propagacion de eventos para un elemento especificado
             * Es equivalente al metodo on de Jquery
             *
             * @param {HtmlElement} element Elemento al que se asigna el evento
             * @param {string} event Evento que se desea asignar
             * @param {string} selector Especifica a que elemento debe responder el evento
             * @param {callback} handler Funcion a realizar por el evento
             *
            */
            _on (element, event, selector, handler) {
                element.addEventListener(event, function(e) {
                    if (e.target.closest(selector)) {
                        handler(e);
                    }
                });
            };

            /**
             * Serializa la información del formulario especificado.
             * Esta es una funcion muy simlar a la de Jquey.
             *
             * @param {HTMLElement} form
            */
            _serialize(form)
            {
                return Array.from(
                    new FormData(form),
                    e => e.map(encodeURIComponent).join('=')
                ).join('&');
            }

            /**
             * Obtiene la propiedades del objeto y las escribe en el formulario de edicion
             *
             * @param {HtmlElement} form
             * @param {Object} errors
            */
            _readFormData(form,object) {
                if (form === undefined) return;

                if( ! form instanceof HTMLElement) return;

                for (let property in object)
                {
                    let field = form.querySelector(`[name="${property}"]`);
                    if (field) {
                        field.value = object[property];
                    }
                }
            }

            /**
             * Muestra los errores en el formulario
             * @param {HtmlElement} form
             * @param {Object} errors
            */
            _readFormErrors(form,errors)
            {
                for (let x in errors) {
                    let input = form.querySelector(`[name='${x}']`);

                    if (input) {
                        input.classList.add('is-invalid')
                        let formGroup = input.parentElement;

                        if(formGroup){
                            let div = formGroup.querySelector('div');

                            if(div){
                                div.innerHTML = errors[x].shift();
                            }
                        }
                    }
                }
            }

            /**
             * Remueve los errores del formulario
             *
             * @param {HTMLElement} form
            */
            _removeFormErrors(form) {
                let elements = Array.from(form.querySelectorAll('.is-invalid'));

                elements.forEach(input => {
                    input.classList.remove('is-invalid');

                    let formGroup = input.parentElement;

                    if(formGroup){
                        let divMessage = formGroup.querySelector('div');
                        if(divMessage){
                            divMessage.innerHTML = '';
                        }
                    }
                })
            }

            /**
             * Registra los eventos del objeto
            */
            _registerEvents() {
                this._on(this._table.container ,this._destroy.event,this._destroy.selector , this.destroy.bind(this));
                this._on(this._table.container ,this._edit.event,this._edit.selector , this.edit.bind(this));
                this._on(document ,this._paginator.event, this._paginator.selector, this.paginate.bind(this));

                this._create.el.addEventListener( this._create.event, this.create.bind(this));
                this._modal.form.el.addEventListener(this._modal.form.event,this.save.bind(this))
            }

            /**
             * Revisa si el objeto tiene una propiedad
            */
            _hasProperty(property, object) {
                return object && object.hasOwnProperty(property);
            }

            /**
             * Revisa si un obeto no tiene propiedades definida
            */
            _isEmptyProperties(properties = {}){
                return Object.getOwnPropertyNames(properties).length === 0;
            }

            /**
             * Asigna un evento con su respectivo nombre
             *
             * @param {string} method
             * @param {string} url
            */
            _setAction( method='', url = '') {
                this._form.el.dataset.method = method;
                this._form.el.dataset.url = url;
            }

            /**
             * Actualiza el texto del buton de envio
             * del formulario.
            */
            _updateTextSubmitButton(name = '')
            {
                this._form.submit.innerText = name;
            }

            /**
             * Limpia el contenido del formulario
            */
            _clearForm(){
                this._form.el.reset();
                this._removeFormErrors(this._form.el)
            }

            /**
             * controla la visibilidad de la pantalla modal
             *
             * @param {Boolean} visibility Indica si muestra o no al elemento.
             * Si no se especifica el parametro, por defecto siempre sera verdadero.
            */
            _displayModal(visibility = true) {
                $(this._modal.el).modal( visibility ? 'show':'hide');
            }

            /**
             * Actualiza el titulo de la ventana modal
             *
             * @param {String} title Titulo a mostrar
            */
            _updateTitleModal(title = '') {
                this._modal.titles.el.innerHTML = title;
            }

            /**
             * Actualiza el contenido que hay en la tabla
             *
             * @param {String} template Contenido html a mostrar
            */
            _renderTable(template = '') {
                this._table.container.innerHTML = template;
            }
        }


        let crudComponent = new CrudComponent(dom)
    </script>
@endsection
