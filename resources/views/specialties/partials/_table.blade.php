<!-- Projects table -->
<table id="table_model" class="table align-items-center table-flush">
    <thead class="thead-light">
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Nombre</th>
        <th scope="col">Descripcion</th>
        <th scope="col">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($collection as $element)
        <tr data-id="{{$element->id}}" data-object="{{ $element }}">
            <td>{{$element->id}}</td>
            <td>{{$element->name}}</td>
            <td>{{$element->description}}</td>
            <td>
                <button class="btn btn-primary btn-sm btn-icon fas fa-pencil-alt" data-action="edit" title="Editar"></button>
                <button class="btn btn-danger  btn-sm btn-icon far fa-trash-alt"  data-action="delete" title="Eliminar"></button>
            </td>
        </tr>
        @empty
            <tr class="no-records-found"><td colspan="{{ 4 }}">No se encontraron resultados</td></tr>
        @endforelse
    </tbody>
</table>

<div class="card-body">
    {{ $collection->links() }}
</div>


