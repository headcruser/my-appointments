<table class="table table-bordered table-striped table-hover" id="table_model">
    <thead>
        <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Alias</th>
            <th>Fecha de creación</th>
            <th class="text-center">Acciones</th>
        </tr>
    </thead>
    <tbody>
        @forelse($collection as $item)
            <tr data-id="{{$item->id}}" data-object="{{ $item }}">
                <td>{{ $item->id }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->display_name }}</td>
                <td>{{ $item->created_at->toFormattedDateString() }}</td>
                <td>
                    <button class="btn btn-primary btn-sm btn-icon fas fa-pencil-alt" data-action="edit" title="Editar"></button>
                    <button class="btn btn-danger  btn-sm btn-icon far fa-trash-alt"  data-action="delete" title="Eliminar"></button>
                </td>
            </tr>
        @empty
            <tr class="no-records-found">
                <td colspan="{{ 5 }}">No se encontraron resultados</td>
            </tr>
        @endforelse
    </tbody>
</table>

<div class="card-body">
    {{ $collection->links() }}
</div>
